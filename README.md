# OpenMC173

![OpenMC173 Logo](assets/logo_128.png)

OpenMC173 is a faithful recreation of version Beta 1.7.3 of Minecraft, written from scratch in the D programming language, and released under the permissive MIT open-source license.

Everything is still in very early development. Contributions are welcome!

The project's primary goals are to:

- Replicate Minecraft Beta v1.7.3 gameplay as accurately as possible.
- Provide flexible modding tools to mod makers, and easy access to mods for players.
- Make quality-of-life improvements to the original game, which may include bug fixes and new features.

Points 1 and 3 might conflict at times, but we'll deal with that stuff on a case-by-case basis.


## Playing

There's not much to play at the moment, so there's no point in providing downloads and installers. If you're really curious to see what's been done so far, check out the development instructions in [CONTRIBUTING.md](CONTRIBUTING.md).


## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md).


## Modding

*Coming Soon™*
