Problem
=======

Rendering 256 chunks with one block per chunk was super slow.


Method
======

Using the `SDL_GetPerformanceCounter()` function, I profiled
`GameScreen.render()` to pinpoint the culprit.

See commit `8ae14f0`for the exact code used.


Findings
========

`Chunk.useVertices()` unsurprisingly took the longest overall, and profiling
inside of that function itself revealed that a massive amount of time was wasted
iterating over the remaining (16×16×128 - 1) blocks of empty space, only to
determine that there was nothing to do. This cost can be seen in the file
`empty_block_cost.csv`, in which each row compares the cost of rendering
the first block of the chunk with the cost of iterating over the remainder of
the blocks.


Solution
========

I added a `numRenderable` field inside of the struct, which keeps track of the
number of renderable blocks in the chunk (for now, that's just the number of
non-Air blocks).

The performance was once again measured (using the same method as above), and
the results can be seen in `empty_block_cost_fast.csv`.


Limitations
===========

This method won't help in the case that the final block in the chunk is occupied
but all others are empty. In that case, the performance wouldn't be affected at
all.

One possible solution to this problem is to keep track of a "first
renderable block" index.

Another possible solution is to split up chunks into 16×16×16 "sections", giving
each section its own `numRenderable` field.

Above all else, though: the optimization I wrote here only helps skip empty bits
of the world. If the chunk were full of blocks, this optimization wouldn't have
helped at all. Perhaps it would be best to cache a "facesVisible" array, keeping
track of which faces of each block are occluded.


Improvements
============

A future improvement could be a `Chunk.isEmpty()` method returning false if
`numRenderable == 0`. This would allow `GameScreen.render()` to skip an entire
iteration of a chunk loop if an empty chunk is encountered. This could
potentially allow for massive render distances in sparse sky-islands type
worlds.

