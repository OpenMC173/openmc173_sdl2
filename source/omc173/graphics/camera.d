module omc173.graphics.camera;

import omc173.theusual;

import std.math;


/**
 * Describes the position and orientation of a 3D camera, and provides a method
 * for on-demand view matrix calculation.
 */
public struct CameraSpec
{
    /// Position of the camera. Safe to modify.
    vec3f position = vec3f(0, 0, 0);

    /// DO NOT assign values directly. Use `setPitch` instead.
    private float pitch = 0;
    private mat3f pitchMtx;

    // DO NOT assign values directly. Use `setYaw` instead.
    private float yaw = 0;
    private mat3f yawMtx;

    private vec3f cameraXaxis = vec3f(1, 0, 0);
    private vec3f cameraZaxis = vec3f(0, 0, 1);

    this(vec3f position)
    {
        this.position = position;

        setYaw(-3 * PI / 4);
        setPitch(PI / 4);
    }

    public vec3f getCameraXaxis()
    {
        return cameraXaxis;
    }

    public vec3f getCameraZaxis()
    {
        return cameraZaxis;
    }

    /**
     * Returns the pitch of the camera in radians.
     *
     * The pitch is the angle of the about its X axis, that is, the axis
     * perpendicular to the camera's viewing direction and global Y axis.
     */
    public float getPitch()
    {
        return pitch;
    }

    /**
     * Sets the camera pitch angle in radians.
     *
     * The provided angle is clamped to the range [-π/2.001, π/2.001]
     * (the actual range boundaries are slightly smaller than π/2 because
     * pointing the camera straight up or down leads to odd behavior.
     */
    public void setPitch(float pitch)
    {
        this.pitch = pitch.clamp(-PI / 2.001f, PI / 2.001f);
        pitchMtx = mat3f.rotation(this.pitch, cameraXaxis);
    }

    /**
     * Returns the pitch of the camera in radians.
     */
    public float getYaw()
    {
        return yaw;
    }

    /**
     * Sets the camera yaw angle in radians.
     *
     * The provided angle is tweaked to be within the range [0, 2×π).
     * So, if you pass in (9/4 π), the stored value will be (1/4 π), and if you
     * pass in (-1/4 π), the stored value will be (7/4 π).
     */
    public void setYaw(float yaw)
    out(; this.yaw >= 0)
    out(; this.yaw < 2 * PI)
    do
    {
        this.yaw = (yaw % (2 * PI)) + ((yaw < 0.0) ? (2 * PI) : 0.0);
        yawMtx = mat3f.rotateY(this.yaw);
        cameraXaxis = (yawMtx * vec3f(1, 0, 0));
        cameraZaxis = yawMtx * vec3f(0, 0, 1);
    }
    unittest
    {
        writeln("TEST: setYaw()");

        import std.math : approxEqual;

        CameraSpec camera1 = CameraSpec(vec3f(0,0,0));
        CameraSpec camera2 = CameraSpec(vec3f(0,0,0));

        camera1.setYaw(- PI / 4);
        camera2.setYaw(7 * PI / 4);
        assert(approxEqual(camera1.getYaw(), camera2.getYaw()));

        camera1.setYaw(9 * PI / 4);
        camera2.setYaw(PI / 4);
        assert(approxEqual(camera1.getYaw(), camera2.getYaw()));

        for (float θ = 0.0; θ < 2 * PI; θ += 0.001)
        {
            camera1.setYaw(θ);
        }

        for (float θ = 0.0; θ > 2 * PI; θ -= 0.001)
        {
            camera1.setYaw(θ);
        }
    }

    /**
     * Returns the view matrix of this camera.
     */
    public mat4f viewMatrix()
    {
        vec3f cameraDir = pitchMtx * yawMtx * vec3f(0.0f, 0.0f, -1.0f);
        mat4f view = mat4f.lookAt(position, position + cameraDir, vec3f(0.0, 1.0, 0.0));
        return view;
    }
}
