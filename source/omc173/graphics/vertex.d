module omc173.graphics.vertex;

import gfm.math;

import omc173.resources.texture_packs;


struct VertexSpec
{
    // The names of these fields must match the names of attributes in the
    // vertex shader.
    vec3f position = vec3f(0, 0, 0);
    vec2f texCoord = vec2f(0, 0);
    vec2f texSrcPos = vec2f(0, 0);
    vec2f texSrcSize = vec2f(0, 0);

    // Get the names of fields as an array. Hopefully they're always returned in
    // the same order in which they're defined.
    import std.traits : FieldNameTuple;
    enum ATTR_NAMES = FieldNameTuple!VertexSpec;

    this(vec3f position, vec2f texCoord, vec2f texSrcPos, vec2f texSrcSize)
    {
        this.position = position;
        this.texCoord = texCoord;
        this.texSrcPos = texSrcPos;
        this.texSrcSize = texSrcSize;
    }

    this(vec3f position, vec2f texCoord, Texture texture)
    {
        this.position = position;
        this.texCoord = texCoord;
        setTexture(texture);
    }

    this(vec3f position, vec2f texCoord)
    {
        this.position = position;
        this.texCoord = texCoord;
    }

    public void setTexture(Texture texture)
    {
        this.texSrcPos = texture.srcPos;
        this.texSrcSize = texture.srcSize;
    }
}

public void addVertex(VertexSpec[] vertices, VertexSpec toAdd, ref size_t idx)
{
    vertices[idx] = toAdd;
    idx++;
}
