module omc173.ndarray;

import omc173.theusual;

import std.algorithm.iteration;
import std.algorithm.searching;
import std.conv : to;
import std.range : iota, repeat;
import std.string : format;
import std.traits;
import std.typecons : reverse, tuple;

import omc173.util : staticIota;


/// A generic n-dimensional array type.
///
/// Basic Usage:
///
///     NDarray!(int, 3, 4, 5) myArray;
///     myArray.setData( [...] );
///     assert(myArray.CAPACITY == 3 * 4 * 5);
///     int[] flatView = myArray.flat;
///     int last = myArray[2, 3, 4];
///
///     foreach (x, y, z, ref element; myArray)
///     {
///         element = x + y + z;
///     }
///
///     foreach (element; myArray)
///     {
///         assert(element >= 0);
///     }
///
///
/// Advanced Usage:
///
///     - axisOrder: Used to tell the struct how to interpret the underlying 1D
///                  array in N-dimensions. See documentation of `axisOrder` for
///                  details.
///
/// Known Issues:
///     - an NDarray of type `char` seems to be a bit buggy, I think because of
///       UTF encoding and the fact that char.init == 'xFF'. Not sure if wchar
///       and dchar have the same issues.
///
/// TODO: Can we make this work with range functions? Perhaps via `alias this`?
public struct NDarray(E, DIMENSIONS...)
if
(
    DIMENSIONS.length > 0 &&
    all!((e) => isImplicitlyConvertible!(typeof(e), size_t))([DIMENSIONS]) &&
    all!((e) => e > 0)([DIMENSIONS])
)
{
    enum N = DIMENSIONS.length;
    enum CAPACITY = [ DIMENSIONS ].fold!((size_t a, size_t b) => a * b);
    private static immutable(size_t[N]) dimensions = [ DIMENSIONS ];

    private E[CAPACITY] data;
    alias RawType = typeof(data);

    /// Maps an array level (0 = outermost, $ = innermost) to a position in [DIMENSIONS].
    ///
    /// Describes the semantic order of the internal flat array's axes.
    /// In 2D arrays, this would be called "row-major" vs "column-major" order.
    /// By default, it's assumed that the "outermost" axis has a length of
    /// DIMENSIONS[0] and the "innermost" axis has a length of DIMENSIONS[$-1].
    /// Note that this makes an NDarray declaration look like a backwards
    /// "vanilla" multi-dimensional array declaration:
    ///     int[11][5][3] foo;
    /// has the same memory layout as:
    ///     NDarray!(int, 3, 5, 11) foo;
    /// This has the nice benefit of making NDarray declaration syntax match
    /// the opIndex syntax:
    ///     NDarray!(int, 3, 5, 11) foo;
    ///     foo[2, 4, 10];  // gets the last element of the array.
    /// However, the ordering can be overridden if the underlying data
    /// is stored in an order that is not intuitive (e.g. a cube where the
    /// innermost axis represents the Y coordinate of the cube). In this case,
    /// axisOrder comes to the rescue. If N = 3 and axisOrder = [1, 2, 0], then
    /// the innermost axis is actually DIMENSIONS[1] in size, the next-innermost
    /// axis is DIMENSIONS[2] in size, and the outermost axis is DIMENSIONS[0]
    /// in size.
    public size_t[N] axisOrder = staticIota!(size_t, N);
    invariant (axisOrder[].all!((e) => e < N));
    invariant (axisOrder[].uniq.count == axisOrder.length);

    this(size_t[N] axisOrder)
    do
    {
        this.axisOrder = axisOrder;
    }

    /// Sets the contents of this array to the contents of `data`.
    /// `data` may not be larger than `this.CAPACITY`, but it may be smaller.
    /// If it is smaller, then only the contents in the range [0..`data.length`)
    /// will be overwritten, and the remaining contents will be unchanged.
    public void setData(E[] data)
    in(data.length <= CAPACITY)
    do
    {
        this.data[0..data.length] = data;
        this.axisOrder = axisOrder;
    }

    /**
     * Returns a 1-D slice of the NDarray contents.
     *
     * Points at the actual array contents, so modifying the returned slice
     * will modify the contents of the NDarray.
     */
    public E[] flat()
    {
        E[] retVal = data;
        return retVal;
    }

    /// Is there a better way of doing this?
    public const(E[]) flatConst() const
    {
        const(E[]) retVal = data;
        return retVal;
    }


    // Declarations of E get(...) for all possible values of N.
    // The 2D/3D versions are just what generateOpIndexFunction() would return,
    // except the parameter names are x, y, z instead of idx0, idx1, idx2.
    static if (N == 1)
    {
        ref E opIndex(size_t i)
        {
            return data[i];
        }
    }
    else static if (N == 2)
    {
        ref E opIndex(size_t x, size_t y)
        {
            size_t[N] params = [x, y];
            size_t idx;
            idx += params[axisOrder[0]] * dimensions[axisOrder[1]];
            idx += params[axisOrder[1]];
            return data[idx];
        }
    }
    else static if (N == 3)
    {
        ref E opIndex(size_t x, size_t y, size_t z)
        {
            size_t[N] params = [x, y, z];
            size_t idx;
            idx += params[axisOrder[0]] * dimensions[axisOrder[2]] * dimensions[axisOrder[1]];
            idx += params[axisOrder[1]] * dimensions[axisOrder[2]];
            idx += params[axisOrder[2]];
            return data[idx];
        }
    }
    else
    {
        mixin(generateOpIndexFunction(N));
    }


    /// foreach will always iterate in the order the data is stored internally,
    /// irrespective of the value of axisOrder.
    public int opApply(int delegate(ref E) loop)
    {
        int result = 0;
        foreach (ref E element; data)
        {
            result = loop(element);
            if (result != 0) break;
        }
        return result;
    }


    /// ditto
    public int opApplyReverse(int delegate(ref E) loop)
    {
        int result = 0;
        foreach_reverse (ref E element; data)
        {
            result = loop(element);
            if (result != 0) break;
        }
        return result;
    }


    // Indexed versions of foreach for all possible values of N.
    static if (N == 1)
    {
        public int opApply(int delegate(size_t, ref E) loop)
        {
            int result = 0;
            foreach (i, ref E element; data)
            {
                result = loop(i, element);
                if (result != 0) break;
            }
            return result;
        }

        public int opApplyReverse(int delegate(size_t, ref E) loop)
        {
            int result = 0;
            foreach_reverse (i, ref E element; data)
            {
                result = loop(i, element);
                if (result != 0) break;
            }
            return result;
        }
    }
    else
    {
        mixin(generateOpApply(N, false));
        mixin(generateOpApply(N, true));
    }


    /// Returns the multi-dimensional index corresponding to the given index.
    ///
    /// Given a 2*2 array, indexToCoords(2) returns:
    /// [1, 0] when axisOrder is [0, 1] (xy)
    /// [0, 1] when axisOrder is [1, 0] (yx)
    public size_t[N] indexToCoords(size_t i) const
    {
        size_t[N] coords;

        foreach (n; 0..N)
        {
            int sizeOfLowerDims = 1;
            foreach (d; n+1 .. N)
            {
                sizeOfLowerDims *= dimensions[axisOrder[d]];
            }
            coords[axisOrder[n]] = (i % (sizeOfLowerDims * dimensions[axisOrder[n]])) / sizeOfLowerDims;
        }

        return coords;
    }

    public size_t opDollar(size_t pos)()
    {
        return dimensions[pos];
    }
}


/*************************************
 *                                   *
 *     Tests & Helper Functions      *
 *                                   *
 *************************************/

private string generateOpIndexFunction(size_t dimensions)
in (dimensions >= 1)
{
    string def = "ref E opIndex(";
    for (int i = 0; i < dimensions; i++)
    {
        def ~= "size_t idx%d".format(i);
        if (i < dimensions - 1) def ~= ", ";
    }
    def ~= ") { size_t idx; size_t[N] params = [";
    for (int i = 0; i < dimensions; i++)
    {
        def ~= "idx%d".format(i);
        if (i < dimensions - 1) def ~= ", ";
    }
    def ~= "]; ";
    for (int i = 0; i < dimensions; i++)
    {
        def ~= "idx += params[axisOrder[%d]]".format(i);
        for (int j = (dimensions - 1).to!int; j > i; j--)
        {
            def ~= " * dimensions[axisOrder[%d]]".format(j);
        }
        def ~= "; ";
    }
    def ~= "return data[idx]; }";
    return def;
}
unittest
{
    writeln("TEST: generateOpIndexFunction()");
    assert(generateOpIndexFunction(1) == `ref E opIndex(size_t idx0) { size_t idx; size_t[N] params = [idx0]; idx += params[axisOrder[0]]; return data[idx]; }`);
    assert(generateOpIndexFunction(2) == `ref E opIndex(size_t idx0, size_t idx1) { size_t idx; size_t[N] params = [idx0, idx1]; idx += params[axisOrder[0]] * dimensions[axisOrder[1]]; idx += params[axisOrder[1]]; return data[idx]; }`);
    assert(generateOpIndexFunction(3) == `ref E opIndex(size_t idx0, size_t idx1, size_t idx2) { size_t idx; size_t[N] params = [idx0, idx1, idx2]; idx += params[axisOrder[0]] * dimensions[axisOrder[2]] * dimensions[axisOrder[1]]; idx += params[axisOrder[1]] * dimensions[axisOrder[2]]; idx += params[axisOrder[2]]; return data[idx]; }`);
    assert(generateOpIndexFunction(4) == `ref E opIndex(size_t idx0, size_t idx1, size_t idx2, size_t idx3) { size_t idx; size_t[N] params = [idx0, idx1, idx2, idx3]; idx += params[axisOrder[0]] * dimensions[axisOrder[3]] * dimensions[axisOrder[2]] * dimensions[axisOrder[1]]; idx += params[axisOrder[1]] * dimensions[axisOrder[3]] * dimensions[axisOrder[2]]; idx += params[axisOrder[2]] * dimensions[axisOrder[3]]; idx += params[axisOrder[3]]; return data[idx]; }`);
    assert(generateOpIndexFunction(7) == `ref E opIndex(size_t idx0, size_t idx1, size_t idx2, size_t idx3, size_t idx4, size_t idx5, size_t idx6) { size_t idx; size_t[N] params = [idx0, idx1, idx2, idx3, idx4, idx5, idx6]; idx += params[axisOrder[0]] * dimensions[axisOrder[6]] * dimensions[axisOrder[5]] * dimensions[axisOrder[4]] * dimensions[axisOrder[3]] * dimensions[axisOrder[2]] * dimensions[axisOrder[1]]; idx += params[axisOrder[1]] * dimensions[axisOrder[6]] * dimensions[axisOrder[5]] * dimensions[axisOrder[4]] * dimensions[axisOrder[3]] * dimensions[axisOrder[2]]; idx += params[axisOrder[2]] * dimensions[axisOrder[6]] * dimensions[axisOrder[5]] * dimensions[axisOrder[4]] * dimensions[axisOrder[3]]; idx += params[axisOrder[3]] * dimensions[axisOrder[6]] * dimensions[axisOrder[5]] * dimensions[axisOrder[4]]; idx += params[axisOrder[4]] * dimensions[axisOrder[6]] * dimensions[axisOrder[5]]; idx += params[axisOrder[5]] * dimensions[axisOrder[6]]; idx += params[axisOrder[6]]; return data[idx]; }`);
}

/// For generating a call to a loop delegate function with an arbitrary number
/// of index loop variables, such as in the following foreach:
///
/// foreach (x, y, z, ref cell; myNdArray) { ... }
private string generateLoopCall(string loopDelegateName,
                                string idxArrayName,
                                size_t idxArraySize,
                                string lastArgName)
{
    string argList;
    foreach (i; 0..idxArraySize)
    {
        argList ~= "%s[%d], ".format(idxArrayName, i);
    }

    return loopDelegateName ~ "(" ~ argList ~ lastArgName ~ ")";
}
unittest
{
    writeln("TEST: generateLoopCall()");

    assert(generateLoopCall("loop", "coords", 2, "element") == "loop(coords[0], coords[1], element)");
    assert(generateLoopCall("l", "i", 1, "e") == "l(i[0], e)");
    assert(generateLoopCall("l", "i", 2, "e") == "l(i[0], i[1], e)");
    assert(generateLoopCall("l", "i", 3, "e") == "l(i[0], i[1], i[2], e)");
    assert(generateLoopCall("l", "i", 4, "e") == "l(i[0], i[1], i[2], i[3], e)");
    assert(generateLoopCall("l", "i", 5, "e") == "l(i[0], i[1], i[2], i[3], i[4], e)");
}

private string generateOpApplyParamType(size_t n)
{
    return "int delegate(" ~ ("size_t, ".repeat(n).fold!"a~=b"("")) ~ "ref E)";
}
unittest
{
    assert(generateOpApplyParamType(0) == "int delegate(ref E)");
    assert(generateOpApplyParamType(1) == "int delegate(size_t, ref E)");
    assert(generateOpApplyParamType(2) == "int delegate(size_t, size_t, ref E)");
    assert(generateOpApplyParamType(3) == "int delegate(size_t, size_t, size_t, ref E)");
}

private string generateOpApply(size_t n, bool reverse)
{
    // TODO: Once mixin types get released, we can define opApply and opApplyReverse normally, just have the argument as a mixin type.
    return `
    public int opApply%s(%s loop)
    {
        int result = 0;
        foreach%s (i, ref E element; data)
        {
            size_t[N] coords = indexToCoords(i);
            result = %s;
            if (result != 0) break;
        }
        return result;
    }
    `.format(reverse ? "Reverse" : "",
             generateOpApplyParamType(n),
             reverse ? "_reverse" : "",
             generateLoopCall("loop", "coords", n, "element")
            );
    // It's a big-ish function, I can't be bothered to make unittests for this too...
}

unittest
{
    writeln("TEST: NDarray basic");
    NDarray!(int, 16, 128, 16) a;
    assert(a.RawType.stringof == "int[32768]");
    assert(a.data.length == 16 * 128 * 16 && a.data.length == a.CAPACITY);
}

unittest
{
    writeln("TEST: NDarray.flat()");
    NDarray!(int, 2, 5, 11) arr;
    int[] flattened = arr.flat;
    assert(flattened.length == 2 * 5 * 11);
    assert(flattened is arr.data);
}

/// Make sure that an NDarray with N dimensions and element type T has an
/// opIndex with N parameters, each of type T.
unittest
{
    writeln("TEST: NDarray.opIndex() get");

    NDarray!(int, 10)()[0];
    NDarray!(int, 10)()[1];
    static assert(!__traits(compiles, NDarray!(int, 10)()[0, 0]));
    static assert(!__traits(compiles, NDarray!(int, 10)()[0, 0, 0]));
    static assert(!__traits(compiles, NDarray!(int, 10)()[3.4]));

    NDarray!(int, 10, 20)()[0, 0];
    static assert(!__traits(compiles, NDarray!(int, 10, 20)()[0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20)()[0, 0, 0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20)()[3.4, 5.5]));

    NDarray!(int, 10, 20, 30)()[0, 0, 0];
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30)()[0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30)()[0, 0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30)()[0, 0, 0, 0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30)()[3.4, 5.5, 6.6]));

    NDarray!(int, 2, 3, 5, 7, 11)()[0, 0, 0, 0, 0];
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30, 40, 50)()[0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30, 40, 50)()[0, 0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30, 40, 50)()[0, 0, 0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30, 40, 50)()[0, 0, 0, 0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30, 40, 50)()[0, 0, 0, 0, 0, 0]));
    static assert(!__traits(compiles, NDarray!(int, 10, 20, 30, 40, 50)()[3.4, 5.5, 6.6, 7.7, 8.8]));
}

// TODO: Test this with char arrays, it didn't work earlier
unittest
{
    import std.exception : assertThrown;
    import core.exception : RangeError;

    writeln("TEST: NDarray.opIndex() set");

    NDarray!(int, 10) x;
    x[0] = 7;
    assert(x[0] == 7);

    NDarray!(int, 5, 2) y;
    y[0, 0] = 2;
    y[4, 1] = 11;
    assert(y.data[].count!((e) => e != int.init) == 2);

    NDarray!(int, 10, 5, 2) z;
    assert(z.flat.all!(e => e == int.init));
    z[9, 4, 1] = 13;
    assert(z[9, 4, 1] == 13);
    assertThrown!RangeError(z[10, 4, 1]);
    assertThrown!RangeError(z[9, 5, 1]);
    assertThrown!RangeError(z[9, 4, 2]);

    int i = 0;
    for (int a = 0; a < 10; a++)
    {
        for (int b = 0; b < 5; b++)
        {
            for (int c = 0; c < 2; c++)
            {
                z[a, b, c] = i;
                i++;
            }
        }
    }
    assert(z.data == staticIota!(int, 100));
}

unittest
{
    import std.exception : assertThrown;

    writeln("TEST: NDarray.setData()");

    NDarray!(int, 5) a;
    a.setData([2, 3, 5, 7, 11]);
    assert(a.flat == [2, 3, 5, 7, 11]);
    a.setData([4, 8, 16, 32]);
    assert(a.flat == [4, 8, 16, 32, 11]);
    a.setData([99]);
    assert(a.flat == [99, 8, 16, 32, 11]);
}

unittest
{
    writeln("TEST: NDarray axisOrder 2D");

    int[15] dataXY = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ];
    int[15] dataYX = [ 1, 6, 11, 2, 7, 12, 3, 8, 13, 4, 9, 14, 5, 10, 15 ];

    NDarray!(int, 3, 5) arrXY;
    NDarray!(int, 3, 5) arrYX;
    arrXY.setData(dataXY);
    arrYX.setData(dataYX);
    arrYX.axisOrder = [1, 0];

    for (int x = 0; x < 3; x++)
    {
        for (int y = 0; y < 5; y++)
        {
            assert(arrXY[x, y] == arrYX[x, y]);
        }
    }
}

unittest
{
    writeln("TEST: NDarray axisOrder 3D");

    // Consider the following 3D array:
    //
    //
    //            |          11           23
    //            |        10           22
    //            |       9           21
    //            |     8           20
    //   Y axis   |           7           19
    // length = 3 |         6           18
    //            |       5           17
    //            |     4           16
    //            |           3           15       /
    //            |         2           14       /  Z axis
    //            |       1           13       /  length = 4
    //            |     0           12       /
    //
    //
    //                  --------------
    //                      X axis
    //                    length = 2
    //
    // By the default axisOrder (xyz = [0, 1, 2]), data is stored like this:
    // [ 0, 1, 2, 3, 4, 5, 6, ... ]
    //
    // But suppose we got data that looks like this:
    // [ 0, 4, 8, 1, 5, 9, 2, 6, 10, 3, 7, 11, ... ]
    // i.e., what you get when traversing the cuboid first by the y axis, then
    // the z axis, then the x axis (essentially just flipping y and z).
    //
    // You would want to treat the NDarray instance as if the data were arranged
    // in the intuitive xyz order. The solution is to set axisOrder such that
    // the NDarray instance knows what order to traverse the axes in.
    //
    // In this case, we want it to act as if the axes are arranged xzy. (x being
    // the outermost axis, y being the innermost).
    //
    // So we set axisOrder to [0, 2, 1], and then we can access elements the
    // same as we always did.
    //
    int[24] dataXYZ = staticIota!(int, 24);
    int[24] dataXZY = [ 0, 4, 8, 1, 5, 9, 2, 6, 10, 3, 7, 11, 12, 16, 20, 13, 17, 21, 14, 18, 22, 15, 19, 23 ];

    NDarray!(int, 2, 3, 4) arrXYZ;
    NDarray!(int, 2, 3, 4) arrXZY;
    arrXYZ.setData(dataXYZ);
    arrXZY.setData(dataXZY);
    arrXZY.axisOrder = [ 0, 2, 1 ];    // Like writing "x, z, y"

    for (int x = 0; x < 2; x++)
    {
        for (int y = 0; y < 3; y++)
        {
            for (int z = 0; z < 4; z++)
            {
                assert(arrXYZ[x, y, z] == arrXZY[x, y, z]);
            }
        }
    }
}

unittest
{
    writeln("TEST: NDarray axisOrder non-involutory");

    // Now with zxy ordering (axisOrder = [2, 0, 1]):
    // This is important and distinct from the above cases because:
    // Think of axisOrder as a bijective function that maps indices to each
    // other. For a 3D array, there are six such functions, but only two of them
    // are *not* their own inverse (Wikipedia says this is called "not
    // involutory"). There are some parts of the NDarray code which might
    // require the inverse of axisOrder, but which still pass other tests
    // because the axisOrder happens to be involutory. This test will hopefully
    // catch any such cases.
    //
    // (For a 2D array, the only two possible axisOrders are both involutory.)

    NDarray!(int, 2, 3, 4) arr;
    arr.axisOrder = [2, 0, 1];
    arr.setData(staticIota!(int, 2 * 3 * 4));

    // indexToCoords()
    assert(arr.indexToCoords(0)  == [0, 0, 0]);
    assert(arr.indexToCoords(1)  == [0, 1, 0]);
    assert(arr.indexToCoords(2)  == [0, 2, 0]);
    assert(arr.indexToCoords(3)  == [1, 0, 0]);
    assert(arr.indexToCoords(4)  == [1, 1, 0]);
    assert(arr.indexToCoords(5)  == [1, 2, 0]);
    assert(arr.indexToCoords(6)  == [0, 0, 1]);
    assert(arr.indexToCoords(7)  == [0, 1, 1]);
    assert(arr.indexToCoords(8)  == [0, 2, 1]);
    assert(arr.indexToCoords(9)  == [1, 0, 1]);
    assert(arr.indexToCoords(10) == [1, 1, 1]);
    assert(arr.indexToCoords(11) == [1, 2, 1]);
    assert(arr.indexToCoords(12) == [0, 0, 2]);
    assert(arr.indexToCoords(13) == [0, 1, 2]);
    assert(arr.indexToCoords(14) == [0, 2, 2]);
    assert(arr.indexToCoords(15) == [1, 0, 2]);
    assert(arr.indexToCoords(16) == [1, 1, 2]);
    assert(arr.indexToCoords(17) == [1, 2, 2]);
    assert(arr.indexToCoords(18) == [0, 0, 3]);
    assert(arr.indexToCoords(19) == [0, 1, 3]);
    assert(arr.indexToCoords(20) == [0, 2, 3]);
    assert(arr.indexToCoords(21) == [1, 0, 3]);
    assert(arr.indexToCoords(22) == [1, 1, 3]);
    assert(arr.indexToCoords(23) == [1, 2, 3]);

    // opIndex
    int[2*3*4] expectedData = staticIota!(int, 2*3*4);
    int i;
    foreach (z; 0..4)
    {
        foreach (x; 0..2)
        {
            foreach (y; 0..3)
            {
                // TODO: I'm not sure this is a good enough test of opIndex?
                assert(arr[x,y,z] == expectedData[i]);
                i++;
            }
        }
    }
}

unittest
{
    writeln("TEST: NDarray foreach");

    // 1D
    NDarray!(string, 10) arr1;
    string[10] data1 = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"];
    arr1.setData(["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]);
    int i;
    foreach (e; arr1)
    {
        assert(e == data1[i]);
        i++;
    }

    // 2D
    NDarray!(string, 3, 3) arr2;
    string[9] data2 = ["a", "b", "c", "d", "e", "f", "g", "h", "i"];
    arr2.setData(data2);
    i = 0;
    foreach (e; arr2)
    {
        assert(e == data2[i]);
        i++;
    }

    // Changing the axis order shouldn't make a difference:
    arr2.axisOrder = [1, 0];
    i = 0;
    foreach (e; arr2)
    {
        assert(e == data2[i]);
        i++;
    }

    // 3D
    NDarray!(string, 2, 2, 2) arr3;
    string[8] data3 = ["a", "b", "c", "d", "e", "f", "g", "h"];
    arr3.setData(data3);
    i = 0;
    foreach (e; arr3)
    {
        assert(e == data3[i]);
        i++;
    }

    // Changing the axis order shouldn't make a difference:
    arr3.axisOrder = [2, 0, 1];
    i = 0;
    foreach (e; arr3)
    {
        assert(e == data3[i]);
        i++;
    }
}

unittest
{
    writeln("TEST: NDarray foreach_reverse");

    NDarray!(int, 10) arr;
    arr.setData(staticIota!(int, 10));
    int i = 9;
    foreach_reverse (e; arr)
    {
        assert(e == i);
        i--;
    }
}

unittest
{
    writeln("TEST: NDarray foreach ref");

    // Without ref:
    NDarray!(int, 10) arr;
    arr.setData(staticIota!(int, 10));
    foreach (e; arr)
    {
        e++;
    }
    int[10] expected = staticIota!(int, 10);
    int i;
    foreach (e; arr)
    {
        assert(e == expected[i]);
        i++;
    }

    // With ref:
    arr.setData(staticIota!(int, 10));
    foreach (ref e; arr)
    {
        e = e + 1;
    }
    expected = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    i = 0;
    foreach (e; arr)
    {
        assert(e == expected[i]);
        i++;
    }
}

unittest
{
    writeln("TEST: NDarray foreach indexed");

    // 1D
    NDarray!(string, 10) arr1;
    arr1.setData(["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]);
    int iManual = 0;
    foreach (iForeach, e; arr1)
    {
        assert(iManual == iForeach);
        iManual++;
    }


    // 2D
    NDarray!(int, 4, 3) arr2;
    int[2][12] expected2 = [
        [0, 0],
        [0, 1],
        [0, 2],
        [1, 0],
        [1, 1],
        [1, 2],
        [2, 0],
        [2, 1],
        [2, 2],
        [3, 0],
        [3, 1],
        [3, 2],
    ];
    size_t[2][12] actual2;
    int i;
    foreach (x, y, e; arr2)
    {
        actual2[i] = [x, y];
        i++;
    }
    assert(actual2 == expected2);

    // Now with axisOrder = yx:
    arr2.axisOrder = [1, 0];
    expected2 = [
        [0, 0],
        [1, 0],
        [2, 0],
        [3, 0],
        [0, 1],
        [1, 1],
        [2, 1],
        [3, 1],
        [0, 2],
        [1, 2],
        [2, 2],
        [3, 2],
    ];
    i = 0;
    foreach (x, y, e; arr2)
    {
        actual2[i] = [x, y];
        i++;
    }
    assert(actual2 == expected2);

    // No further axisOrder related testing necessary, because the only part of
    // opApply() that touches axisOrder is indexToCoords(), and that has quite
    // a few axisOrder-related tests itself.


    // 3D
    NDarray!(int, 2, 3, 4) arr3;
    int[3][24] expected3 = [
        [0, 0, 0],
        [0, 0, 1],
        [0, 0, 2],
        [0, 0, 3],
        [0, 1, 0],
        [0, 1, 1],
        [0, 1, 2],
        [0, 1, 3],
        [0, 2, 0],
        [0, 2, 1],
        [0, 2, 2],
        [0, 2, 3],
        [1, 0, 0],
        [1, 0, 1],
        [1, 0, 2],
        [1, 0, 3],
        [1, 1, 0],
        [1, 1, 1],
        [1, 1, 2],
        [1, 1, 3],
        [1, 2, 0],
        [1, 2, 1],
        [1, 2, 2],
        [1, 2, 3],
    ];
    size_t[3][24] actual3;
    i = 0;
    foreach (x, y, z, e; arr3)
    {
        actual3[i] = [x, y, z];
        i++;
    }
    assert(actual3 == expected3);
}

unittest
{
    writeln("TEST: NDarray foreach_reverse indexed");

    NDarray!(string, 10) arr;
    arr.setData(["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]);
    int iManual = 9;
    foreach_reverse (iForeach, e; arr)
    {
        assert(iManual == iForeach);
        iManual--;
    }
}

unittest
{
    writeln("TEST: NDarray indexToCoords()");

    // 1D
    NDarray!(int, 10) arr1;
    foreach (i; 0..10)
    {
        assert(arr1.indexToCoords(i) == [i]);
    }


    // 2D
    NDarray!(int, 3, 2) arr2;
    // First with xy ordering (axisOrder = [0,1]):
    assert(arr2.indexToCoords(0) == [0, 0]);
    assert(arr2.indexToCoords(1) == [0, 1]);
    assert(arr2.indexToCoords(2) == [1, 0]);
    assert(arr2.indexToCoords(3) == [1, 1]);
    assert(arr2.indexToCoords(4) == [2, 0]);
    assert(arr2.indexToCoords(5) == [2, 1]);

    // Now with yx ordering (axisOrder = [1, 0]):
    arr2.axisOrder = [1, 0];
    assert(arr2.indexToCoords(0) == [0, 0]);
    assert(arr2.indexToCoords(1) == [1, 0]);
    assert(arr2.indexToCoords(2) == [2, 0]);
    assert(arr2.indexToCoords(3) == [0, 1]);
    assert(arr2.indexToCoords(4) == [1, 1]);
    assert(arr2.indexToCoords(5) == [2, 1]);


    // 3D
    NDarray!(int, 2, 3, 4) arr3;
    // First with xyz ordering (axisOrder = [0,1,2]):
    assert(arr3.indexToCoords(0) ==  [0, 0, 0]);
    assert(arr3.indexToCoords(1) ==  [0, 0, 1]);
    assert(arr3.indexToCoords(2) ==  [0, 0, 2]);
    assert(arr3.indexToCoords(3) ==  [0, 0, 3]);
    assert(arr3.indexToCoords(4) ==  [0, 1, 0]);
    assert(arr3.indexToCoords(5) ==  [0, 1, 1]);
    assert(arr3.indexToCoords(6) ==  [0, 1, 2]);
    assert(arr3.indexToCoords(7) ==  [0, 1, 3]);
    assert(arr3.indexToCoords(8) ==  [0, 2, 0]);
    assert(arr3.indexToCoords(9) ==  [0, 2, 1]);
    assert(arr3.indexToCoords(10) == [0, 2, 2]);
    assert(arr3.indexToCoords(11) == [0, 2, 3]);
    assert(arr3.indexToCoords(12) == [1, 0, 0]);
    assert(arr3.indexToCoords(13) == [1, 0, 1]);
    assert(arr3.indexToCoords(14) == [1, 0, 2]);
    assert(arr3.indexToCoords(15) == [1, 0, 3]);
    assert(arr3.indexToCoords(16) == [1, 1, 0]);
    assert(arr3.indexToCoords(17) == [1, 1, 1]);
    assert(arr3.indexToCoords(18) == [1, 1, 2]);
    assert(arr3.indexToCoords(19) == [1, 1, 3]);
    assert(arr3.indexToCoords(20) == [1, 2, 0]);
    assert(arr3.indexToCoords(21) == [1, 2, 1]);
    assert(arr3.indexToCoords(22) == [1, 2, 2]);
    assert(arr3.indexToCoords(23) == [1, 2, 3]);

    // Now with yxz ordering (axisOrder = [1, 0, 2]):
    arr3.axisOrder = [1, 0, 2];
    assert(arr3.indexToCoords(0) ==  [0, 0, 0]);
    assert(arr3.indexToCoords(1) ==  [0, 0, 1]);
    assert(arr3.indexToCoords(2) ==  [0, 0, 2]);
    assert(arr3.indexToCoords(3) ==  [0, 0, 3]);
    assert(arr3.indexToCoords(4) ==  [1, 0, 0]);
    assert(arr3.indexToCoords(5) ==  [1, 0, 1]);
    assert(arr3.indexToCoords(6) ==  [1, 0, 2]);
    assert(arr3.indexToCoords(7) ==  [1, 0, 3]);
    assert(arr3.indexToCoords(8) ==  [0, 1, 0]);
    assert(arr3.indexToCoords(9) ==  [0, 1, 1]);
    assert(arr3.indexToCoords(10) == [0, 1, 2]);
    assert(arr3.indexToCoords(11) == [0, 1, 3]);
    assert(arr3.indexToCoords(12) == [1, 1, 0]);
    assert(arr3.indexToCoords(13) == [1, 1, 1]);
    assert(arr3.indexToCoords(14) == [1, 1, 2]);
    assert(arr3.indexToCoords(15) == [1, 1, 3]);
    assert(arr3.indexToCoords(16) == [0, 2, 0]);
    assert(arr3.indexToCoords(17) == [0, 2, 1]);
    assert(arr3.indexToCoords(18) == [0, 2, 2]);
    assert(arr3.indexToCoords(19) == [0, 2, 3]);
    assert(arr3.indexToCoords(20) == [1, 2, 0]);
    assert(arr3.indexToCoords(21) == [1, 2, 1]);
    assert(arr3.indexToCoords(22) == [1, 2, 2]);
    assert(arr3.indexToCoords(23) == [1, 2, 3]);
}

unittest
{
    writeln("TEST: NDarray opDollar()");

    // 1D
    NDarray!(int, 5) arr1;
    arr1.setData(staticIota!(int, 5));
    assert(arr1[$ - 1] == arr1[4]);


    // 2D
    NDarray!(int, 2, 3) arr2;
    arr2.setData(staticIota!(int, 6));
    assert(arr2[0, $ - 1] == arr2[0, 2]);
    assert(arr2[$ - 1, 0] == arr2[1, 0]);
    assert(arr2[$ - 1, $ - 1] == arr2[1, 2]);


    // 3D (non-involutory)
    NDarray!(int, 2, 3, 4) arr3;
    arr3.axisOrder = [1, 2, 0];
    arr3.setData(staticIota!(int, 24));
    assert(arr3[0, 0, $ - 1] == arr3[0, 0, 3]);
    assert(arr3[0, $ - 1, 0] == arr3[0, 2, 0]);
    assert(arr3[0, $ - 1, $ - 1] == arr3[0, 2, 3]);
    assert(arr3[$ - 1, 0, 0] == arr3[1, 0, 0]);
    assert(arr3[$ - 1, 0, $ - 1] == arr3[1, 0, 3]);
    assert(arr3[$ - 1, $ - 1, 0] == arr3[1, 2, 0]);
    assert(arr3[$ - 1, $ - 1, $ - 1] == arr3[1, 2, 3]);
}
