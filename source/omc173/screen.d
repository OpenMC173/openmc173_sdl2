module omc173.screen;

import bindbc.sdl.bind;
import bindbc.opengl.bind;

public import gfm.math;


/**
 * An implementation of this interface roughly corresponds to one high-level
 * part of the game.
 */
interface Screen
{
    /**
     * Called once per main game loop, before any rendering is done,
     * but after input and other `SDL_Event`s have been processed.
     *
     * `delta` is the time in seconds since this function was last called.
     */
    void idleUpdate(real delta);

    /**
     * Called once per game loop, after idleUpdate() and glClear(), and just
     * before the buffers are swapped. All rendering should go here.
     */
    void render();

    /**
     * Called when the given key has been pressed.
     */
    void keyDown(SDL_Keysym keysym);

    /**
     * Called when the given key has been released.
     */
    void keyUp(SDL_Keysym keysym);

    /**
     * Called when the mouse moves.
     */
    void mouseMotion(SDL_MouseMotionEvent event);

    /**
     * Called when a mouse button is clicked or released.
     *
     * event.type will be either `SDL_MOUSEBUTTONDOWN` or `SDL_MOUSEBUTTONUP`.
     */
    void mouseButton(SDL_MouseButtonEvent event);

    /**
     * Called when the window is resized.
     *
     * `size` is the new size of the screen. It is equal to the result of
     * calling `getRenderBufferSize()` (defined in main.d), it's just passed
     * in as a parameter here for convenience.
     */
    void resize(vec2i size);

    /**
     * Called when the screen is no longer needed. Cleans up anything that is
     * needed, releases resources, etc.
     *
     * Will be called exactly once, and will be the last method ever called on
     * the current screen instance.
     */
    void finalize();
}
