module omc173.mainmenu.mainmenuscreen;

import omc173.theusual;

import omc173.game.game_screen : GameScreen;
import omc173.graphics.vertex;
import omc173.gui;
import omc173.main;
import omc173.screen;


/**
 * The Main Menu screen.
 */
final class MainMenuScreen : Screen
{
    private Resources res;
    private mat4f mvp;

    private GuiButton btn_ssp;
    private GuiButton btn_smp;
    private GuiButton btn_textures;
    private GuiButton btn_options;

    private GuiButton[4] buttons;

    private GuiButton hoveringOver;

    private vec2f menu_bg_tile_size = vec2f(64, 64); // TODO: Get this from the image size.
    private vec2i num_menu_bg_tiles;

    private GuiImage title_left;
    private GuiImage title_right;
    private GuiImage background;


    this(Resources res)
    {
        this.res = res;

        SDL_SetRelativeMouseMode(SDL_FALSE).chkX;

        glEnable(GL_DEPTH_TEST);
        glDisable(GL_CULL_FACE);

        btn_ssp = createMenuButton(res,"Singleplayer", () { goToScreen(new GameScreen(res)); });
        btn_smp = createMenuButton(res, "Multiplayer", (){});
        btn_textures = createMenuButton(res, "Mods and Texture Packs", (){});
        btn_options = createMenuButton(res, "Options...", (){});
        buttons = [ btn_ssp, btn_smp, btn_textures, btn_options ];

        background = new GuiImage(
            box2i(0,0,0,0),
            -100,
            res.textures.gui.menuBackground,
            res.shaders["2D"],
            vec4f(64/256f, 64/256f, 64/256f, 1f),
            vec2i(1, 1),
            vec2f(1.0f, -1.0f),
        );

        title_left = new GuiImage(
            box2i(vec2i(0, 0), res.textures.gui.menuLogoLeft.srcSize.to!vec2i),
            0,
            res.textures.gui.menuLogoLeft,
            res.shaders["2D"],
        );

        title_right = new GuiImage(
            box2i(vec2i(0, 0), res.textures.gui.menuLogoRight.srcSize.to!vec2i),
            0,
            res.textures.gui.menuLogoRight,
            res.shaders["2D"],
        );

        vec2i size = getRenderBufferSize();
        recalcMvp(size);
        doLayout(size);
    }

    override void idleUpdate(real delta)
    {

    }

    override void render()
    {
        background.render(mvp);
        title_left.render(mvp);
        title_right.render(mvp);
        btn_ssp.render(mvp);
        btn_smp.render(mvp);
        btn_textures.render(mvp);
        btn_options.render(mvp);
    }

    override void keyDown(SDL_Keysym keysym)
    {

    }

    override void keyUp(SDL_Keysym keysym)
    {

    }

    override void mouseMotion(SDL_MouseMotionEvent event)
    {
        vec2i mousePos = vec2i(event.x, event.y);
        // Convert to y-up:
        mousePos.y = getRenderBufferSize().y - mousePos.y;

        import std.algorithm.searching : maxElement;
        import std.algorithm.sorting : partition;
        import std.range.primitives : empty;

        GuiButton[] candidates = buttons[].partition!((e) => !e.rect.contains(mousePos));
        if (!candidates.empty)
        {
            if (hoveringOver !is null)
            {
                hoveringOver.hovering = false;
            }
            hoveringOver = candidates.maxElement!((e) => e.z);
            hoveringOver.hovering = true;
        }
        else if (hoveringOver !is null)
        {
            hoveringOver.hovering = false;
            hoveringOver = null;
        }
    }

    override void mouseButton(SDL_MouseButtonEvent event)
    {
        // Only acknowledge button releases. Should figure out a better way to
        // handle proper "clicks". Right now, you can click somewhere random,
        // move the mouse over a button, release, and it will act as if you
        // clicked the button which is a bit weird.
        if (event.type == SDL_MOUSEBUTTONUP)
        {
            if (hoveringOver !is null)
            {
                if  (hoveringOver.on_click !is null)
                {
                    hoveringOver.on_click();
                }
                else
                {
                    Log.i("GuiButton \"" ~ hoveringOver.label ~ "\" has null on_click callback.");
                }
            }
        }
    }

    override void resize(vec2i size)
    {
        recalcMvp(size);
        doLayout(size);
    }

    override void finalize()
    {

    }

    private void doLayout(vec2i screenSize)
    {
        int btn_distanceFromBottom = screenSize.y - 96 - (screenSize.y / 4);
        int button_spacing = 8;
        int button_height = 40;

        btn_ssp.rect = btn_ssp.rect.topAt(vec2i(screenSize.x / 2, btn_distanceFromBottom - 0 * (button_height + button_spacing)));
        btn_smp.rect = btn_smp.rect.topAt(vec2i(screenSize.x / 2, btn_distanceFromBottom - 1 * (button_height + button_spacing)));
        btn_textures.rect = btn_textures.rect.topAt(vec2i(screenSize.x / 2, btn_distanceFromBottom - 2 * (button_height + button_spacing)));
        btn_options.rect = btn_options.rect.topAt(vec2i(screenSize.x / 2, btn_distanceFromBottom - 3 * (button_height + button_spacing)));

        background.numTiles = vec2i(
            screenSize.x / menu_bg_tile_size.x.to!int + 1,
            screenSize.y / menu_bg_tile_size.y.to!int + 1
        );

        background.rect = rectangle(
            0,
            (getRenderBufferSize().y - menu_bg_tile_size.y).to!int,
            menu_bg_tile_size.x.to!int,
            menu_bg_tile_size.y.to!int
        );

        int logoYOffset = screenSize.y - 60;
        int logoCenteringOffset = (256 * (title_left.texture.srcSize.x - title_right.texture.srcSize.x)).to!int;
        int rightHalf_left = (screenSize.x / 2f).to!int + logoCenteringOffset;
        int leftHalf_left = rightHalf_left - (256 * title_left.texture.srcSize.x * 2).to!int;
        title_left.rect = rectangle(
            leftHalf_left,
            logoYOffset - (256 * 2 * title_left.texture.srcSize.y).to!int,
            (256 * 2 * title_left.texture.srcSize.x).to!int,
            (256 * 2 * title_left.texture.srcSize.y).to!int
        );
        title_right.rect = rectangle(
            rightHalf_left,
            logoYOffset - (256 * 2 * title_right.texture.srcSize.y).to!int,
            (256 * 2 * title_right.texture.srcSize.x).to!int,
            (256 * 2 * title_right.texture.srcSize.y).to!int
        );
    }

    private void recalcMvp(vec2i screenSize)
    {
        mat4f view = mat4f.lookAt(
            vec3f(0, 0, 100.5),
            vec3f(0, 0, 0),
            vec3f(0, 1, 0)
        );
        mat4f proj = mat4f.orthographic(0, screenSize.x, 0, screenSize.y, 0, 201);
        mvp = proj * view;
    }
}
