module omc173.game.game_screen;

import std.algorithm.iteration;

import omc173.theusual;
import omc173.game.chunk;
import omc173.game.block;
import omc173.main;
import omc173.mainmenu.mainmenuscreen : MainMenuScreen;
import omc173.screen;
import omc173.graphics;


/**
 * The screen shown while playing the actual game (with voxels and whatnot).
 */
final class GameScreen : Screen
{
    private Resources res;

    private Shader shader;

    private uint vbo;

    // Current size of the VBO
    private int vboCapacity = 42_467_328; // Enough for 16x16 chunks, each filled between [0..64]
    invariant(vboCapacity % VertexSpec.sizeof == 0);

    /// If the VBO needs to be resized, resize it by this much. Don't resize it
    /// to just fit the current vertices, because then it will need to be
    /// resized every time a block is added.
    /// Currently the enum has the value 129600 which is enough for 100
    /// fully-rendered cubes (i.e. all faces exposed).
    private enum int VBO_CAPACITY_INCREMENT = 100 * Chunk.VERTICES_PER_CUBE * VertexSpec.sizeof;

    private uint textureID;
    private CameraSpec camera;

    private float cameraSpeed = 10;
    private bool cameraMoveLeft;
    private bool cameraMoveRight;
    private bool cameraMoveForward;
    private bool cameraMoveBack;
    private bool cameraMoveUp;
    private bool cameraMoveDown;

    private float farClip = 1024.0f;

    // Minor slowdown at 48x48, but 32x32 works great
    private enum NUM_CHUNKS_X = 4;
    private enum NUM_CHUNKS_Z = 4;
    private NDarray!(Chunk, NUM_CHUNKS_X, NUM_CHUNKS_Z) chunks;

    this(Resources res)
    {
        this.res = res;
        shader = res.shaders["cube"];
        textureID = res.textures.blocks.stone.id;

        camera = CameraSpec(vec3f(-3, -3, -3));

        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);

        // We do the following in order:
        // 1. Generate a Buffer object, and store its ID/address in "vbo".
        // 2. Bind it, allowing us to perform other operations on it.
        // 3. Using glBufferData(), set the initial contents of the VBO. The
        //    contents are defined in the local variable "vertices".
        //    We set the "usage" parameter to DYNAMIC_DRAW since we plan on
        //    changing the position of one of the vertices (see idleUpdate()).
        //    "initial_vertices" is an array of VertexSpec structs. This allows
        //    us to define each vertex in a human-readable manner, but preserve
        //    the simple float-array structure OpenGL likes.
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);

        glBufferData(
            GL_ARRAY_BUFFER,
            Chunk.VERTICES_CAPACITY * VertexSpec.sizeof,
            null,
            GL_DYNAMIC_DRAW
        );

        SDL_SetRelativeMouseMode(SDL_TRUE).chkX;

        Log.i("Initializing chunks...");

        foreach (cx, cz, ref chunk; chunks)
        {
            chunk.chunkCoord = vec2i(cx.to!int, cz.to!int);

            if (cx < NUM_CHUNKS_X - 1)
                chunk.neighbourPosX = &(chunks[cx + 1, cz]);

            if (cx > 0)
                chunk.neighbourNegX = &(chunks[cx - 1, cz]);

            if (cz < NUM_CHUNKS_Z - 1)
                chunk.neighbourPosZ = &(chunks[cx, cz + 1]);

            if (cz > 0)
                chunk.neighbourNegZ = &(chunks[cx, cz - 1]);

            for (int x = 0; x < Chunk.SIZE_X; x++)
            {
                for (int z = 0; z < Chunk.SIZE_Z; z++)
                {
                    chunk.setBlock(Block.BEDROCK, x, 0, z);

                    for (int y = 1; y < 60; y++)
                    {
                        chunk.setBlock(Block.STONE, x, y, z);
                    }

                    for (int y = 60; y < 63; y++)
                    {
                        chunk.setBlock(Block.DIRT, x, y, z);
                    }

                    chunk.setBlock(Block.GRASS, x, 63, z);

                    if (x == 0 && z == 0) chunk.setBlock(Block.PLANKS, x, 65, 0);
                }
            }
        }
    }

    override void idleUpdate(real delta)
    {
        // Move camera
        if (cameraMoveLeft ^ cameraMoveRight)
        {
            if (cameraMoveLeft)
            {
                camera.position -= camera.getCameraXaxis() * cameraSpeed * delta;
                //camera.position.x -= cameraSpeed * delta;
            }
            else
            {
                camera.position += camera.getCameraXaxis() * cameraSpeed * delta;
                //camera.position.x += cameraSpeed * delta;
            }
        }

        if (cameraMoveForward ^ cameraMoveBack)
        {
            if (cameraMoveForward)
            {
                camera.position -= camera.getCameraZaxis() * cameraSpeed * delta;
                //camera.position.z -= cameraSpeed * delta;
            }
            else
            {
                camera.position += camera.getCameraZaxis() * cameraSpeed * delta;
                //camera.position.z += cameraSpeed * delta;
            }
        }

        if (cameraMoveUp ^ cameraMoveDown)
        {
            if (cameraMoveUp)
            {
                camera.position += vec3f(0, 1, 0) * cameraSpeed * delta;
            }
            else
            {
                camera.position -= vec3f(0, 1, 0) * cameraSpeed * delta;
            }
        }


        // glBufferSubData modifies the contents of the VBO.
        // We want to replace the whole array, so the start offset should be 0,
        // and the size should be the size of our vertices array in bytes
        // (It's a dynamic array, so we need to do length * size of each elmt).
        //glBindBuffer(GL_ARRAY_BUFFER, vbo);
        //glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.length * VertexSpec.sizeof, vertices.ptr);
    }

    size_t numVertices;

    override void render()
    {
        bool hasAnyoneChanged = false;
        for (int x = 0; x < NUM_CHUNKS_X; x++)
        {
            for (int z = 0; z < NUM_CHUNKS_Z; z++)
            {
                hasAnyoneChanged |= chunks[x, z].hasChanged();
            }
        }

        if (hasAnyoneChanged)
        {
            // Gather all chunks' vertex arrays:

            static VertexSpec[] allVerticesFlat;
            NDarray!(VertexSpec[], NUM_CHUNKS_X, NUM_CHUNKS_Z) allVertices;

            foreach (x, z, ref chunk; chunks)
            {
                allVertices[x, z] = chunk.getVertices(res);
            }

            numVertices = fold!((acc, e) => acc + e.length)(allVertices.flatConst, 0UL);


            // Flatten vertices into a 1D array:

            if (allVerticesFlat is null) allVerticesFlat = new VertexSpec[numVertices];
            else                           allVerticesFlat.length = numVertices;

            size_t start_idx = 0;
            size_t end_idx;
            foreach (vertex_array; allVertices)
            {
                end_idx = start_idx + vertex_array.length;
                allVerticesFlat[start_idx .. end_idx] = vertex_array[];
                start_idx = end_idx;
            }


            // Update VBO:

            glBindBuffer(GL_ARRAY_BUFFER, vbo);

            int vertices_byte_size = (numVertices * VertexSpec.sizeof).to!int;
            if (vboCapacity < vertices_byte_size)
            {
                // Resize the vbo
                int minimumSizeIncrease = vertices_byte_size - vboCapacity;
                int sizeIncrease = roundUpTo(minimumSizeIncrease, VBO_CAPACITY_INCREMENT);
                int new_capacity = vboCapacity + sizeIncrease;
                Log.i("Increasing VBO capacity by %,3d bytes, from %,3d to %,3d".format(sizeIncrease, vboCapacity, new_capacity));
                vboCapacity = new_capacity;
                glBufferData(GL_ARRAY_BUFFER, vboCapacity, null, GL_DYNAMIC_DRAW);
            }
            glBufferSubData(GL_ARRAY_BUFFER, 0, vertices_byte_size, allVerticesFlat.ptr);
        }

        glUseProgram(shader.id);

        mat4f model = mat4f.identity();
        mat4f view = camera.viewMatrix();
        mat4f proj = mat4f.perspective(
            radians(45.0f),
            (getRenderBufferSize().x).to!float / getRenderBufferSize().y,
            0.01f,
            farClip
        );
        mat4f mvp = proj * view * model;

        // The third variable, transpose, must be TRUE!!!!
        // I spent AGES trying to figure out why everything looked so funky...
        // ;_;
        glUniformMatrix4fv(shader.uniform("mvp"), 1, true, mvp.ptr);

        glUniform4f(shader.uniform("multColor"), 1, 1, 1, 1);

        // Use texture unit 0 for the following operations
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureID);
        glUniform1i(shader.uniform("texture"), 0);  // corresponds to texture unit 0

        foreach (attr; VertexSpec.ATTR_NAMES)
        {
            glEnableVertexAttribArray(shader.attr(attr));
        }


        // The following lines specify the arrays to use for those attributes.

        // We are now using a single VBO to store all attributes.
        // First, we need to bind the VBO as usual. Then, a static foreach loop
        // generates calls to glVertexAttribPointer() for each attribute.
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        static foreach (attr; VertexSpec.ATTR_NAMES)
        {
            glVertexAttribPointer(
                shader.attr(attr),
                mixin("VertexSpec." ~ attr ~ ".sizeof") / float.sizeof,
                GL_FLOAT,
                GL_FALSE,
                VertexSpec.sizeof,
                cast(void*)(mixin("VertexSpec." ~ attr ~ ".offsetof"))
            );
        }

        // This line says "Draw triangles based on the above arrays."
        glDrawArrays(GL_TRIANGLES, 0, numVertices.to!int);

        foreach (attr; VertexSpec.ATTR_NAMES)
        {
            glDisableVertexAttribArray(shader.attr(attr));
        }
    }

    override void keyDown(SDL_Keysym keysym)
    {
        switch (keysym.sym)
        {
            case SDLK_ESCAPE:
                goToScreen(new MainMenuScreen(res));
                break;

            case SDLK_w:
                cameraMoveForward = true;
                break;

            case SDLK_s:
                cameraMoveBack = true;
                break;

            case SDLK_a:
                cameraMoveLeft = true;
                break;

            case SDLK_d:
                cameraMoveRight = true;
                break;

            case SDLK_SPACE:
                cameraMoveUp = true;
                break;

            case SDLK_LSHIFT:
                cameraMoveDown = true;
                break;

            case SDLK_r:
                bool isRMM = SDL_GetRelativeMouseMode().to!bool;
                SDL_SetRelativeMouseMode(isRMM ? SDL_FALSE : SDL_TRUE).chkX;
                break;

            default:
                break;
        }
    }

    override void keyUp(SDL_Keysym keysym)
    {
        switch (keysym.sym)
        {
            case SDLK_w:
                cameraMoveForward = false;
                break;

            case SDLK_s:
                cameraMoveBack = false;
                break;

            case SDLK_a:
                cameraMoveLeft = false;
                break;

            case SDLK_d:
                cameraMoveRight = false;
                break;

            case SDLK_SPACE:
                cameraMoveUp = false;
                break;

            case SDLK_LSHIFT:
                cameraMoveDown = false;
                break;

            default:
                break;
        }
    }

    override void mouseMotion(SDL_MouseMotionEvent event)
    {
        enum YAW_SPEED =   1 / 500f;
        enum PITCH_SPEED = 1 / 500f;

        if (SDL_GetRelativeMouseMode())
        {
            camera.setYaw(camera.getYaw() - event.xrel * YAW_SPEED);
            camera.setPitch(camera.getPitch() - event.yrel * PITCH_SPEED);
        }
    }

    override void mouseButton(SDL_MouseButtonEvent event)
    {

    }

    override void resize(vec2i size)
    {

    }

    override void finalize()
    {
        glDeleteBuffers(1, &vbo);
    }
}
