module omc173.game.chunk;

import omc173.theusual;

import std.array : staticArray;
import std.traits : EnumMembers;

import omc173.game.block;
import omc173.graphics;
import omc173.main;


private alias ChunkArr(T) = NDarray!(T, Chunk.SIZE_X, Chunk.SIZE_Y, Chunk.SIZE_Z);

struct Chunk
{
    enum int SIZE_X = 16;
    enum int SIZE_Y = 128;
    enum int SIZE_Z = 16;

    enum VERTICES_PER_CUBE = 6 * 2 * 3;
    enum VERTICES_CAPACITY = SIZE_X * SIZE_Y * SIZE_Z * VERTICES_PER_CUBE;

    private static Enumap!(Direction, VertexSpec[6]) faces = enumap(
        Direction.X_NEG, [
            VertexSpec(vec3f(0, 1, 0), vec2f(0, 0)),
            VertexSpec(vec3f(0, 0, 0), vec2f(0, 1)),
            VertexSpec(vec3f(0, 0, 1), vec2f(1, 1)),
            VertexSpec(vec3f(0, 1, 0), vec2f(0, 0)),
            VertexSpec(vec3f(0, 0, 1), vec2f(1, 1)),
            VertexSpec(vec3f(0, 1, 1), vec2f(1, 0)),
        ].staticArray,

        Direction.X_POS, [
            VertexSpec(vec3f(1, 1, 1), vec2f(0, 0)),
            VertexSpec(vec3f(1, 0, 1), vec2f(0, 1)),
            VertexSpec(vec3f(1, 0, 0), vec2f(1, 1)),
            VertexSpec(vec3f(1, 1, 1), vec2f(0, 0)),
            VertexSpec(vec3f(1, 0, 0), vec2f(1, 1)),
            VertexSpec(vec3f(1, 1, 0), vec2f(1, 0)),
        ].staticArray,

        Direction.Y_NEG, [
            VertexSpec(vec3f(0, 0, 1), vec2f(0, 1)),
            VertexSpec(vec3f(0, 0, 0), vec2f(0, 0)),
            VertexSpec(vec3f(1, 0, 0), vec2f(1, 0)),
            VertexSpec(vec3f(0, 0, 1), vec2f(0, 1)),
            VertexSpec(vec3f(1, 0, 0), vec2f(1, 0)),
            VertexSpec(vec3f(1, 0, 1), vec2f(1, 1)),
        ].staticArray,

        Direction.Y_POS, [
            VertexSpec(vec3f(0, 1, 0), vec2f(0, 0)),
            VertexSpec(vec3f(0, 1, 1), vec2f(0, 1)),
            VertexSpec(vec3f(1, 1, 1), vec2f(1, 1)),
            VertexSpec(vec3f(0, 1, 0), vec2f(0, 0)),
            VertexSpec(vec3f(1, 1, 1), vec2f(1, 1)),
            VertexSpec(vec3f(1, 1, 0), vec2f(1, 0)),
        ].staticArray,

        Direction.Z_NEG, [
            VertexSpec(vec3f(1, 1, 0), vec2f(0, 0)),
            VertexSpec(vec3f(1, 0, 0), vec2f(0, 1)),
            VertexSpec(vec3f(0, 0, 0), vec2f(1, 1)),
            VertexSpec(vec3f(1, 1, 0), vec2f(0, 0)),
            VertexSpec(vec3f(0, 0, 0), vec2f(1, 1)),
            VertexSpec(vec3f(0, 1, 0), vec2f(1, 0)),
        ].staticArray,

        Direction.Z_POS, [
            VertexSpec(vec3f(0, 1, 1), vec2f(0, 0)),
            VertexSpec(vec3f(0, 0, 1), vec2f(0, 1)),
            VertexSpec(vec3f(1, 0, 1), vec2f(1, 1)),
            VertexSpec(vec3f(0, 1, 1), vec2f(0, 0)),
            VertexSpec(vec3f(1, 0, 1), vec2f(1, 1)),
            VertexSpec(vec3f(1, 1, 1), vec2f(1, 0)),
        ].staticArray,
    );

    /// The number of vertices actually contained in the above array.
    private static uint numVertices = 0;

    // TODO: change axis order to [0, 2, 1] when loading from mcr?
    private ChunkArr!Block blocks/* = ChunkArr!Block([0, 2, 1])*/;

    /// `true` entries indicate blocks that are both:
    /// - Not air (or some other block that doesn't need to be rendered)
    /// - Have at least one face exposed (that is, at least one face neighbours a non-opaque cube-shaped block)
    private ChunkArr!bool renderable;

    /// Caches number of true entries in `renderable`.\
    private int numBlocksRenderable = 0;
    invariant(numBlocksRenderable >= 0, "numBlocksRenderable may not be negative");
    invariant(numBlocksRenderable <= SIZE_X * SIZE_Y * SIZE_Z, "numBlocksRenderable should be at most " ~ (SIZE_X * SIZE_Y * SIZE_Z).to!string);
    version(unittest) invariant
    {
        // This invariant check is super slow, since its runtime is proportional
        // to the total volume of blocks in the chunk. Every time setExposed
        // runs, this will have to run. So it's in a version(unittest) block, so
        // that regular debug builds aren't slowed down so much.
        import std.algorithm.searching : count;
        assert(count(renderable.flatConst, true) == numBlocksRenderable);
    }

    public vec2i chunkCoord;

    public Chunk* neighbourPosX;
    public Chunk* neighbourNegX;
    public Chunk* neighbourPosZ;
    public Chunk* neighbourNegZ;

    public VertexSpec[] mesh;

    /// If true, then the `blocks` array has changed since the last time the
    /// chunk's vertex mesh as generated, and it needs to be re-created.
    private bool needsMeshing;

    /// Sets the block at the chunk's inner [x,y,z] coordinates to type `block`.
    /// The given coordinates are local to the chunk, so they must range from
    /// 0 to `SIZE_X`, `SIZE_Y`, and `SIZE_Z`.
    public void setBlock(Block block, int x, int y, int z)
    in (x >= 0 && x < SIZE_X, "X must be in the range [0, SIZE_X)")
    in (y >= 0 && y < SIZE_Y, "Y must be in the range [0, SIZE_Y)")
    in (z >= 0 && z < SIZE_Z, "Z must be in the range [0, SIZE_Z)")
    do
    {
        Block oldBlock = blocks[x, y, z];
        blocks[x, y, z] = block;
        setExposed(x, y, z);
        if (oldBlock != block) needsMeshing = true;
    }
    ///
    unittest
    {
        writeln("TEST: setBlock()");

        import std.algorithm.searching;

        Chunk chunk;
        assert(all!((b) => b == Block.AIR)(chunk.blocks.flat));
        assert(chunk.numBlocksRenderable == 0);
        assert(chunk.needsMeshing == false);

        // Setting to renderable and non-renderable types:

        chunk.setBlock(Block.STONE, 0, 0, 0);
        assert(count!((b) => b != Block.AIR)(chunk.blocks.flat) == 1);
        assert(chunk.numBlocksRenderable == 1);
        assert(chunk.needsMeshing);

        chunk.setBlock(Block.DIRT, 0, 1, 0);
        assert(count!((b) => b != Block.AIR)(chunk.blocks.flat) == 2);
        assert(chunk.numBlocksRenderable == 2);

        chunk.setBlock(Block.PLANKS, 2, 2, 2);
        assert(count!((b) => b != Block.AIR)(chunk.blocks.flat) == 3);
        assert(chunk.numBlocksRenderable == 3);

        chunk.setBlock(Block.AIR, 0, 0, 0);
        assert(count!((b) => b != Block.AIR)(chunk.blocks.flat) == 2);
        assert(chunk.numBlocksRenderable == 2);

        chunk.setBlock(Block.AIR, 0, 1, 0);
        assert(count!((b) => b != Block.AIR)(chunk.blocks.flat) == 1);
        assert(chunk.numBlocksRenderable == 1);

        chunk.setBlock(Block.AIR, 2, 2, 2);
        assert(count!((b) => b != Block.AIR)(chunk.blocks.flat) == 0);
        assert(chunk.numBlocksRenderable == 0);


        // Completely surround an otherwise renderable block:

        chunk.setBlock(Block.STONE, 1, 1, 1);
        assert(chunk.numBlocksRenderable == 1);
        chunk.setBlock(Block.STONE, 0, 1, 1);
        chunk.setBlock(Block.STONE, 2, 1, 1);
        chunk.setBlock(Block.STONE, 1, 0, 1);
        chunk.setBlock(Block.STONE, 1, 2, 1);
        chunk.setBlock(Block.STONE, 1, 1, 0);
        chunk.setBlock(Block.STONE, 1, 1, 2);
        assert(count!((b) => b != Block.AIR)(chunk.blocks.flat) == 7);
        assert(chunk.numBlocksRenderable == 6);
    }

    private void setExposed(int x, int y, int z)
    in (x >= 0 && x < SIZE_X, "X must be in the range [0, SIZE_X)")
    in (y >= 0 && y < SIZE_Y, "Y must be in the range [0, SIZE_Y)")
    in (z >= 0 && z < SIZE_Z, "Z must be in the range [0, SIZE_Z)")
    do
    {
        import std.algorithm.searching : all;
        import std.traits : EnumMembers;

        Enumap!(Direction, Block) neighbours = enumap(
            Direction.X_POS, x < SIZE_X - 1 ? blocks[x + 1, y, z] : Block.AIR,
            Direction.X_NEG, x > 0          ? blocks[x - 1, y, z] : Block.AIR,
            Direction.Y_POS, y < SIZE_Y - 1 ? blocks[x, y + 1, z] : Block.AIR,
            Direction.Y_NEG, y > 0          ? blocks[x, y - 1, z] : Block.AIR,
            Direction.Z_POS, z < SIZE_Z - 1 ? blocks[x, y, z + 1] : Block.AIR,
            Direction.Z_NEG, z > 0          ? blocks[x, y, z - 1] : Block.AIR,
        );

        immutable bool oldRenderable = renderable[x, y, z];
        renderable[x, y, z] = blocks[x, y, z] != Block.AIR &&
                              !all!((b) => blockInfo[b].renderable)(neighbours.byValue);

        immutable bool newRenderable = renderable[x, y, z];
        immutable bool renderabilityChanged = newRenderable ^ oldRenderable;

        if (renderabilityChanged)
        {
            if (newRenderable) numBlocksRenderable++;
            else               numBlocksRenderable--;

            if (x > 0)          setExposed(x - 1, y, z);
            if (x < SIZE_X - 1) setExposed(x + 1, y, z);
            if (y > 0)          setExposed(x, y - 1, z);
            if (y < SIZE_Y - 1) setExposed(x, y + 1, z);
            if (z > 0)          setExposed(x, y, z - 1);
            if (z < SIZE_Z - 1) setExposed(x, y, z + 1);
        }
    }

    public bool hasChanged()
    {
        return needsMeshing;
    }

    /// Returns a mesh of Vertices used to draw this chunk.
    public VertexSpec[] getVertices(Resources res)
    {
        if (needsMeshing) makeMesh(res);
        return mesh;
    }

    /// Recreates the mesh of vertices representing this chunk.
    ///
    /// There is only one array of vertices that is re-used by each Chunk.
    /// This array is huge in size (16 * 16 * 128 blocks, times 36 vertices per
    /// block, times 36 bytes per VertexSpec instance ~= 42 megabytes).
    ///
    /// Yet, it seems to increase memory usage by >300 MB...
    /// If the "static" is changed to "__gshared", memory usage drops back to 20
    /// ????
    private void makeMesh(Resources res)
    {
        /// This array is used to store work-in-progress meshes, before placinqg them
        /// in a chunk-specific dynamic array (`mesh`). I expect this might be
        /// faster than potentially having to constantly resize and re-allocated
        /// `mesh`, but I haven't thought about it too hard.
        static VertexSpec[Chunk.VERTICES_CAPACITY] vertices;

        numVertices = 0;

        float offsetX = chunkCoord.x * SIZE_X;
        float offsetZ = chunkCoord.y * SIZE_Z;

        int numBlocksRendered = 0;
        bool inEmptyBlock = false;

        outer:
        for (int x = 0; x < SIZE_X; x++)
        {
            for (int z = 0; z < SIZE_Z; z++)
            {
                for (int y = 0; y < SIZE_Y; y++)
                {
                    if (numBlocksRendered >= numBlocksRenderable) break outer;

                    Block block = blocks[x, y, z];
                    if (!(renderable[x, y, z]))
                    {
                        inEmptyBlock = true;
                        continue;
                    }

                    inEmptyBlock = false;

                    numBlocksRendered++;

                    Enumap!(Direction, Block) neighbours = enumap(

                        Direction.X_POS,
                            x < SIZE_X - 1 ? blocks[x + 1, y, z] : (neighbourPosX !is null ? neighbourPosX.blocks[0, y, z] : Block.AIR),

                        Direction.X_NEG,
                            x > 0          ? blocks[x - 1, y, z] : (neighbourNegX !is null ? neighbourNegX.blocks[$ - 1, y, z] : Block.AIR),

                        Direction.Y_POS,
                            y < SIZE_Y - 1 ? blocks[x, y + 1, z] : Block.AIR,

                        Direction.Y_NEG,
                            y > 0          ? blocks[x, y - 1, z] : Block.AIR,

                        Direction.Z_POS,
                            z < SIZE_Z - 1 ? blocks[x, y, z + 1] : (neighbourPosZ !is null ? neighbourPosZ.blocks[x, y, z] : Block.AIR),

                        Direction.Z_NEG,
                            z > 0          ? blocks[x, y, z - 1] : (neighbourNegZ !is null ? neighbourNegZ.blocks[x, y, $ - 1] : Block.AIR),
                    );

                    if (blockInfo[block].shape == Shape.CROSS)
                    {

                    }
                    else if (blockInfo[block].shape == Shape.CUBE)
                    {
                        // There doesn't seem to be any performance difference
                        // when using a static foreach here.
                        foreach (dir; EnumMembers!Direction)
                        {
                            if (blockInfo[neighbours[dir]].opaque) continue;

                            Texture texture = getTexture(res, block, 0, dir);

                            // This slice-assign thing is faster than
                            // manually assigning each field inside of the
                            // upcoming loop.
                            // BUT:
                            // Somehow, setting the texSrcPos and texSrcSize
                            // inside of the loop is actually faster if we
                            // also set tex_coord inside of the
                            // loop. This might have something to do with
                            // CPU caching? I'm not sure, and the speed
                            // improvement is < 5% so nothing dramatic.
                            vertices[numVertices .. numVertices + 6] = faces[dir][0..$];

                            foreach (i; numVertices .. numVertices + 6)
                            {
                                // Setting texSrcPos and texSrcSize x and y
                                // coordinates directly, instead of through
                                // the function, is much faster.
                                vertices[i].texSrcPos.x = texture.srcPos.x;
                                vertices[i].texSrcPos.y = texture.srcPos.y;
                                vertices[i].texSrcSize.x = texture.srcSize.x;
                                vertices[i].texSrcSize.y = texture.srcSize.y;

                                vertices[i].position.x += x + offsetX;
                                vertices[i].position.y += y;
                                vertices[i].position.z += z + offsetZ;
                            }
                            numVertices += 6;
                        }
                    }
                    else
                    {

                    }
                }
            }
        }

        // If `mesh` hasn't been allocated yet, then do so, otherwise just
        // resize it.
        if (mesh is null)
        {
            mesh = new VertexSpec[numVertices];
        }
        else
        {
            mesh.length = numVertices;
        }

        // Then copy the contents of `vertices` into `mesh`.
        // It *must* say `mesh[]`, not just `mesh`. Without the brackets,
        // `mesh` merely becomes a slice of `vertices`, but with the brackets,
        // we actually set the contents of the allocated array.
        mesh[] = vertices[0..numVertices];

        needsMeshing = false;
    }
}
