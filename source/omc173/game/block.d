module omc173.game.block;

import omc173.util : Direction;
import omc173.resources;


enum Block : ubyte
{
    AIR = 0,
    STONE,
    GRASS,
    DIRT,
    COBBLE,
    PLANKS,
    SAPLING,
    BEDROCK,
    WATER_STILL,
    WATER_FLOW,

    LAVA_STILL,
    LAVA_FLOW,
    SAND,
    GRAVEL,
    ORE_GOLD,
    ORE_IRON,
    ORE_COAL,
    LOG,
    LEAVES,
    SPONGE,

    GLASS,
    ORE_LAPIS,
    BLOCK_LAPIS,
    DISPENSER,
    SANDSTONE,
    NOTEBLOCK,
    BED,
    RAIL_POWERED,
    RAIL_DETECTOR,
    PISTON_STICKY,

    COBWEB,
    TALL_GRASS,
    DEAD_SHRUB,
    PISTON,
    PISTON_HEAD,
    WOOL,
    UNKNOWN,
    FLOWER_YELLOW,
    FLOWER_RED,
    MUSHROOM_BROWN,

    MUSHROOM_RED,
    BLOCK_GOLD,
    BLOCK_IRON,
    DOUBLE_SLAB,
    SINGLE_SLAB,
    BRICK,
    TNT,
    BOOKSHELF,
    MOSSY_COBBLE,
    OBSIDIAN,

    TORCH,
    FIRE,
    MOB_SPAWNER,
    STAIR_WOOD,
    CHEST,
    REDSTONE_WIRE,
    ORE_DIAMOND,
    BLOCK_DIAMOND,
    CRAFTING_TABLE,
    CROP_WHEAT,

    FARMLAND,
    FURNACE_OFF,
    FURNACE_ON,
    SIGN_GROUND,
    DOOR_WOOD,
    LADDER,
    RAIL,
    STAIR_COBBLE,
    SIGN_WALL,
    LEVER,

    PRESSURE_PLATE_STONE,
    DOOR_IRON,
    PRESSURE_PLATE_WOOD,
    ORE_REDSTONE_OFF,
    ORE_REDSTONE_ON,
    REDSTONE_TORCH_OFF,
    REDSTONE_TORCH_ON,
    BUTTON_STONE,
    SNOW_LAYER,
    ICE,

    SNOW_BLOCK,
    CACTUS,
    CLAY,
    SUGARCANE,
    JUKEBOX,
    FENCE,
    PUMPKIN,
    NETHERRACK,
    SOUL_SAND,
    GLOWSTONE,

    PORTAL,
    JACK_O_LANTERN,
    CAKE,
    REPEATER_OFF,
    REPEATER_ON,
    CHEST_PHONY,
    TRAPDOOR,
}

enum Shape
{
    CUBE,
    CROSS,
    OTHER
}

struct BlockInfo
{
    string name;
    bool renderable;
    bool opaque;
    Shape shape;
}

static immutable BlockInfo[Block.max + 1] blockInfo = [
    Block.AIR:                  BlockInfo("Air",          false, false, Shape.OTHER ),
    Block.STONE:                BlockInfo("Stone",        true,  true,  Shape.CUBE  ),
    Block.GRASS:                BlockInfo("Grass",        true,  true,  Shape.CUBE  ),
    Block.DIRT:                 BlockInfo("Dirt",         true,  true,  Shape.CUBE  ),
    Block.COBBLE:               BlockInfo("Cobblestone",  true,  true,  Shape.CUBE  ),
    Block.PLANKS:               BlockInfo("Planks",       true,  true,  Shape.CUBE  ),
    Block.SAPLING:              BlockInfo("Sapling",      true,  false, Shape.CROSS ),
    Block.BEDROCK:              BlockInfo("Bedrock",      true,  true,  Shape.CUBE  ),
    Block.WATER_STILL:          BlockInfo(),
    Block.WATER_FLOW:           BlockInfo(),

    Block.LAVA_STILL:           BlockInfo(),
    Block.LAVA_FLOW:            BlockInfo(),
    Block.SAND:                 BlockInfo(),
    Block.GRAVEL:               BlockInfo(),
    Block.ORE_GOLD:             BlockInfo(),
    Block.ORE_IRON:             BlockInfo(),
    Block.ORE_COAL:             BlockInfo(),
    Block.LOG:                  BlockInfo(),
    Block.LEAVES:               BlockInfo(),
    Block.SPONGE:               BlockInfo(),

    Block.GLASS:                BlockInfo(),
    Block.ORE_LAPIS:            BlockInfo(),
    Block.BLOCK_LAPIS:          BlockInfo(),
    Block.DISPENSER:            BlockInfo(),
    Block.SANDSTONE:            BlockInfo(),
    Block.NOTEBLOCK:            BlockInfo(),
    Block.BED:                  BlockInfo(),
    Block.RAIL_POWERED:         BlockInfo(),
    Block.RAIL_DETECTOR:        BlockInfo(),
    Block.PISTON_STICKY:        BlockInfo(),

    Block.COBWEB:               BlockInfo(),
    Block.TALL_GRASS:           BlockInfo(),
    Block.DEAD_SHRUB:           BlockInfo(),
    Block.PISTON:               BlockInfo(),
    Block.PISTON_HEAD:          BlockInfo(),
    Block.WOOL:                 BlockInfo(),
    Block.UNKNOWN:              BlockInfo(),
    Block.FLOWER_YELLOW:        BlockInfo(),
    Block.FLOWER_RED:           BlockInfo(),
    Block.MUSHROOM_BROWN:       BlockInfo(),

    Block.MUSHROOM_RED:         BlockInfo(),
    Block.BLOCK_GOLD:           BlockInfo(),
    Block.BLOCK_IRON:           BlockInfo(),
    Block.DOUBLE_SLAB:          BlockInfo(),
    Block.SINGLE_SLAB:          BlockInfo(),
    Block.BRICK:                BlockInfo(),
    Block.TNT:                  BlockInfo(),
    Block.BOOKSHELF:            BlockInfo(),
    Block.MOSSY_COBBLE:         BlockInfo(),
    Block.OBSIDIAN:             BlockInfo(),

    Block.TORCH:                BlockInfo(),
    Block.FIRE:                 BlockInfo(),
    Block.MOB_SPAWNER:          BlockInfo(),
    Block.STAIR_WOOD:           BlockInfo(),
    Block.CHEST:                BlockInfo(),
    Block.REDSTONE_WIRE:        BlockInfo(),
    Block.ORE_DIAMOND:          BlockInfo(),
    Block.BLOCK_DIAMOND:        BlockInfo(),
    Block.CRAFTING_TABLE:       BlockInfo(),
    Block.CROP_WHEAT:           BlockInfo(),

    Block.FARMLAND:             BlockInfo(),
    Block.FURNACE_OFF:          BlockInfo(),
    Block.FURNACE_ON:           BlockInfo(),
    Block.SIGN_GROUND:          BlockInfo(),
    Block.DOOR_WOOD:            BlockInfo(),
    Block.LADDER:               BlockInfo(),
    Block.RAIL:                 BlockInfo(),
    Block.STAIR_COBBLE:         BlockInfo(),
    Block.SIGN_WALL:            BlockInfo(),
    Block.LEVER:                BlockInfo(),

    Block.PRESSURE_PLATE_STONE: BlockInfo(),
    Block.DOOR_IRON:            BlockInfo(),
    Block.PRESSURE_PLATE_WOOD:  BlockInfo(),
    Block.ORE_REDSTONE_OFF:     BlockInfo(),
    Block.ORE_REDSTONE_ON:      BlockInfo(),
    Block.REDSTONE_TORCH_OFF:   BlockInfo(),
    Block.REDSTONE_TORCH_ON:    BlockInfo(),
    Block.BUTTON_STONE:         BlockInfo(),
    Block.SNOW_LAYER:           BlockInfo(),
    Block.ICE:                  BlockInfo(),

    Block.SNOW_BLOCK:           BlockInfo(),
    Block.CACTUS:               BlockInfo(),
    Block.CLAY:                 BlockInfo(),
    Block.SUGARCANE:            BlockInfo(),
    Block.JUKEBOX:              BlockInfo(),
    Block.FENCE:                BlockInfo(),
    Block.PUMPKIN:              BlockInfo(),
    Block.NETHERRACK:           BlockInfo(),
    Block.SOUL_SAND:            BlockInfo(),
    Block.GLOWSTONE:            BlockInfo(),

    Block.PORTAL:               BlockInfo(),
    Block.JACK_O_LANTERN:       BlockInfo(),
    Block.CAKE:                 BlockInfo(),
    Block.REPEATER_OFF:         BlockInfo(),
    Block.REPEATER_ON:          BlockInfo(),
    Block.CHEST_PHONY:          BlockInfo(),
    Block.TRAPDOOR:             BlockInfo(),
];

public Texture getTexture(Resources res, Block block, ubyte metadata = 0, Direction direction = Direction.X_POS)
{
    final switch (block)
    {
        case Block.AIR:                   throw new Exception("ERROR: Cannot retrieve texture for air.");

        case Block.STONE:                 return res.textures.blocks.stone;

        case Block.GRASS:
        {
            switch (direction)
            {
                case Direction.Y_POS:   return res.textures.blocks.grassTop;
                case Direction.Y_NEG:   return res.textures.blocks.grassBottom;
                default:                return res.textures.blocks.grassSide;
            }
        }

        case Block.DIRT:                  return res.textures.blocks.dirt;
        case Block.COBBLE:                return res.textures.blocks.cobblestone;
        case Block.PLANKS:                return res.textures.blocks.planksOak;

        case Block.SAPLING:
        {
            switch (metadata)
            {
                case 1:     return res.textures.blocks.saplingSpruce;
                case 2:     return res.textures.blocks.saplingBirch;
                default:    return res.textures.blocks.saplingOak;
            }
        }

        case Block.BEDROCK:               return res.textures.blocks.bedrock;

        case Block.WATER_STILL:           return res.textures.blocks.grassSide;
        case Block.WATER_FLOW:            return res.textures.blocks.grassSide;
        case Block.LAVA_STILL:            return res.textures.blocks.grassSide;
        case Block.LAVA_FLOW:             return res.textures.blocks.grassSide;
        case Block.SAND:                  return res.textures.blocks.grassSide;
        case Block.GRAVEL:                return res.textures.blocks.grassSide;
        case Block.ORE_GOLD:              return res.textures.blocks.grassSide;
        case Block.ORE_IRON:              return res.textures.blocks.grassSide;
        case Block.ORE_COAL:              return res.textures.blocks.grassSide;
        case Block.LOG:                   return res.textures.blocks.grassSide;
        case Block.LEAVES:                return res.textures.blocks.grassSide;
        case Block.SPONGE:                return res.textures.blocks.grassSide;
        case Block.GLASS:                 return res.textures.blocks.grassSide;
        case Block.ORE_LAPIS:             return res.textures.blocks.grassSide;
        case Block.BLOCK_LAPIS:           return res.textures.blocks.grassSide;
        case Block.DISPENSER:             return res.textures.blocks.grassSide;
        case Block.SANDSTONE:             return res.textures.blocks.grassSide;
        case Block.NOTEBLOCK:             return res.textures.blocks.grassSide;
        case Block.BED:                   return res.textures.blocks.grassSide;
        case Block.RAIL_POWERED:          return res.textures.blocks.grassSide;
        case Block.RAIL_DETECTOR:         return res.textures.blocks.grassSide;
        case Block.PISTON_STICKY:         return res.textures.blocks.grassSide;
        case Block.COBWEB:                return res.textures.blocks.grassSide;
        case Block.TALL_GRASS:            return res.textures.blocks.grassSide;
        case Block.DEAD_SHRUB:            return res.textures.blocks.grassSide;
        case Block.PISTON:                return res.textures.blocks.grassSide;
        case Block.PISTON_HEAD:           return res.textures.blocks.grassSide;
        case Block.WOOL:                  return res.textures.blocks.grassSide;
        case Block.UNKNOWN:               return res.textures.blocks.grassSide;
        case Block.FLOWER_YELLOW:         return res.textures.blocks.grassSide;
        case Block.FLOWER_RED:            return res.textures.blocks.grassSide;
        case Block.MUSHROOM_BROWN:        return res.textures.blocks.grassSide;
        case Block.MUSHROOM_RED:          return res.textures.blocks.grassSide;
        case Block.BLOCK_GOLD:            return res.textures.blocks.grassSide;
        case Block.BLOCK_IRON:            return res.textures.blocks.grassSide;
        case Block.DOUBLE_SLAB:           return res.textures.blocks.grassSide;
        case Block.SINGLE_SLAB:           return res.textures.blocks.grassSide;
        case Block.BRICK:                 return res.textures.blocks.grassSide;
        case Block.TNT:                   return res.textures.blocks.grassSide;
        case Block.BOOKSHELF:             return res.textures.blocks.grassSide;
        case Block.MOSSY_COBBLE:          return res.textures.blocks.grassSide;
        case Block.OBSIDIAN:              return res.textures.blocks.grassSide;
        case Block.TORCH:                 return res.textures.blocks.grassSide;
        case Block.FIRE:                  return res.textures.blocks.grassSide;
        case Block.MOB_SPAWNER:           return res.textures.blocks.grassSide;
        case Block.STAIR_WOOD:            return res.textures.blocks.grassSide;
        case Block.CHEST:                 return res.textures.blocks.grassSide;
        case Block.REDSTONE_WIRE:         return res.textures.blocks.grassSide;
        case Block.ORE_DIAMOND:           return res.textures.blocks.grassSide;
        case Block.BLOCK_DIAMOND:         return res.textures.blocks.grassSide;
        case Block.CRAFTING_TABLE:        return res.textures.blocks.grassSide;
        case Block.CROP_WHEAT:            return res.textures.blocks.grassSide;
        case Block.FARMLAND:              return res.textures.blocks.grassSide;
        case Block.FURNACE_OFF:           return res.textures.blocks.grassSide;
        case Block.FURNACE_ON:            return res.textures.blocks.grassSide;
        case Block.SIGN_GROUND:           return res.textures.blocks.grassSide;
        case Block.DOOR_WOOD:             return res.textures.blocks.grassSide;
        case Block.LADDER:                return res.textures.blocks.grassSide;
        case Block.RAIL:                  return res.textures.blocks.grassSide;
        case Block.STAIR_COBBLE:          return res.textures.blocks.grassSide;
        case Block.SIGN_WALL:             return res.textures.blocks.grassSide;
        case Block.LEVER:                 return res.textures.blocks.grassSide;
        case Block.PRESSURE_PLATE_STONE:  return res.textures.blocks.grassSide;
        case Block.DOOR_IRON:             return res.textures.blocks.grassSide;
        case Block.PRESSURE_PLATE_WOOD:   return res.textures.blocks.grassSide;
        case Block.ORE_REDSTONE_OFF:      return res.textures.blocks.grassSide;
        case Block.ORE_REDSTONE_ON:       return res.textures.blocks.grassSide;
        case Block.REDSTONE_TORCH_OFF:    return res.textures.blocks.grassSide;
        case Block.REDSTONE_TORCH_ON:     return res.textures.blocks.grassSide;
        case Block.BUTTON_STONE:          return res.textures.blocks.grassSide;
        case Block.SNOW_LAYER:            return res.textures.blocks.grassSide;
        case Block.ICE:                   return res.textures.blocks.grassSide;
        case Block.SNOW_BLOCK:            return res.textures.blocks.grassSide;
        case Block.CACTUS:                return res.textures.blocks.grassSide;
        case Block.CLAY:                  return res.textures.blocks.grassSide;
        case Block.SUGARCANE:             return res.textures.blocks.grassSide;
        case Block.JUKEBOX:               return res.textures.blocks.grassSide;
        case Block.FENCE:                 return res.textures.blocks.grassSide;
        case Block.PUMPKIN:               return res.textures.blocks.grassSide;
        case Block.NETHERRACK:            return res.textures.blocks.grassSide;
        case Block.SOUL_SAND:             return res.textures.blocks.grassSide;
        case Block.GLOWSTONE:             return res.textures.blocks.grassSide;
        case Block.PORTAL:                return res.textures.blocks.grassSide;
        case Block.JACK_O_LANTERN:        return res.textures.blocks.grassSide;
        case Block.CAKE:                  return res.textures.blocks.grassSide;
        case Block.REPEATER_OFF:          return res.textures.blocks.grassSide;
        case Block.REPEATER_ON:           return res.textures.blocks.grassSide;
        case Block.CHEST_PHONY:           return res.textures.blocks.grassSide;
        case Block.TRAPDOOR:              return res.textures.blocks.grassSide;
    }
}
