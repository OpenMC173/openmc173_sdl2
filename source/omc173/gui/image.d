module omc173.gui.image;

import omc173.theusual;

import omc173.gui;
import omc173.graphics.vertex;

class GuiImage : GuiElement
{
    public Texture texture;
    public Shader shader;
    public vec2i numTiles;
    public vec2f offsetMultiplier;
    public vec4f multColor;

    /// A GuiElement that renders a `Texture`. May be drawn repeatedly in a tile
    /// pattern.
    ///
    /// Params:
    ///
    ///     rect = The position of the images's bottom-left corner (if drawing
    ///            multiple instances, this is the bottom-left corner of the
    ///            first instance).
    ///
    ///     z = Z-index of the rendered image.
    ///
    ///     texture = The `Texture` instance to be used.
    ///
    ///     shader = TODO: Remove? Automatically use `2D`?
    ///
    ///     numTiles = The shape of the grid of tiled textures to draw. For
    ///                example, a value of `vec2i(3, 2)` draws 6 instances of
    ///                the image - 3 columns and 2 rows. Default `vec2i(1, 1)`.
    ///
    ///     offsetMultiplier = If tiling, this indicates by how much each
    ///                        instance should be shifted from the previous.
    ///                        Default `vec2f(1, 1)` = no spacing between tiles.
    ///
    ///     multColor = Rendered image will be multiplied by this color.
    ///                 Default vec4f(1, 1, 1, 1) = white.
    ///
    this(
        box2i rect,
        int z,
        Texture texture,
        Shader shader,
        vec4f multColor = vec4f(1.0f, 1.0f, 1.0f, 1.0f),
        vec2i numTiles = vec2i(1, 1),
        vec2f offsetMultiplier = vec2f(1.0f, 1.0f),
    )
    {
        super(rect, z);

        this.texture = texture;
        this.shader = shader;
        this.multColor = multColor;
        this.numTiles = numTiles;
        this.offsetMultiplier = offsetMultiplier;
    }

    public override void render(mat4f mvp)
    {
        VertexSpec[6] vertices = [
            VertexSpec(vec3f(rect.min.x, rect.max.y, z), vec2f(0, 0), texture),
            VertexSpec(vec3f(rect.min.x, rect.min.y, z), vec2f(0, 1), texture),
            VertexSpec(vec3f(rect.max.x, rect.min.y, z), vec2f(1, 1), texture),
            VertexSpec(vec3f(rect.min.x, rect.max.y, z), vec2f(0, 0), texture),
            VertexSpec(vec3f(rect.max.x, rect.min.y, z), vec2f(1, 1), texture),
            VertexSpec(vec3f(rect.max.x, rect.max.y, z), vec2f(1, 0), texture),
        ];

        glUseProgram(shader.id);

        glUniformMatrix4fv(shader.uniform("mvp"), 1, true, mvp.ptr);
        glUniform4fv(shader.uniform("multColor"), 1, multColor.ptr);
        glUniform1i(shader.uniform("columns"), numTiles.x);
        glUniform2f(shader.uniform("offset"), rect.width * offsetMultiplier.x, rect.height * offsetMultiplier.y);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture.id);
        glUniform1i(shader.uniform("texture"), 0);

        foreach (attr; VertexSpec.ATTR_NAMES)
        {
            glEnableVertexAttribArray(shader.attr(attr));
        }

        static foreach (attr; VertexSpec.ATTR_NAMES)
        {
            glVertexAttribPointer(
                shader.attr(attr),
                mixin("VertexSpec." ~ attr ~ ".sizeof") / float.sizeof,
                GL_FLOAT,
                GL_FALSE,
                VertexSpec.sizeof,
                cast(void*)(mixin("VertexSpec." ~ attr ~ ".offsetof") + cast(void*)(vertices.ptr))
            );
        }

        glDrawArraysInstanced(GL_TRIANGLES, 0, vertices.length.to!int, numTiles.x * numTiles.y);

        foreach (attr; VertexSpec.ATTR_NAMES)
        {
            glDisableVertexAttribArray(shader.attr(attr));
        }
    }
}
