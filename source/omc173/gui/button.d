module omc173.gui.button;

import omc173.theusual;

import omc173.gui;
import omc173.graphics.vertex;


/// Can do something when clicked.
/// Can have an image or text or a tooltip or any combination of those three.
class GuiButton : GuiElement
{
    private static vec4f TEXT_COLOR = vec4f(224/256f, 224/256f, 224/256f, 1);
    private static vec4f TEXT_SHADOW_COLOR = vec4f(56/256f, 56/256f, 56/256f, 1);
    private static vec4f TEXT_HOVER_COLOR = vec4f(255/256f, 255/256f, 160/256f, 1);
    private static vec4f TEXT_HOVER_SHADOW_COLOR = vec4f(63/256f, 63/256f, 40/256f, 1);
    private static vec4f TEXT_DISABLED_COLOR = vec4f(160/256f, 160/256f, 160/256f, 1);
    private static vec4f TEXT_DISABLED_SHADOW_COLOR = vec4f(40/256f, 40/256f, 40/256f, 1);

    public Texture texture_default;
    public Texture texture_hover;
    public Texture texture_disabled;
    public string label;
    public string tooltip;
    public Shader shader;
    public Font font;
    public bool enabled = true;
    public bool hovering = false;
    public void delegate() on_click;

    private VertexSpec[] text_vertices;
    private VertexSpec[] text_shadow_vertices;


    this(
        box2i rect,
        int z,
        Texture texture_default,
        Texture texture_hover,
        Texture texture_disabled,
        string label,
        string tooltip,
        bool enabled,
        Shader shader,
        Font font,
        void delegate() on_click
    )
    {
        super(rect, z);

        this.texture_default = texture_default;
        this.texture_hover = texture_hover;
        this.texture_disabled = texture_disabled;
        this.label = label;
        this.tooltip = tooltip;
        this.enabled = enabled;
        this.shader = shader;
        this.font = font;
        this.on_click = on_click;
    }

    public override void render(mat4f mvp)
    {
        Texture texture = !enabled ? texture_disabled :
                          hovering ? texture_hover :
                          texture_default;

        VertexSpec[6] vertices = [
            VertexSpec(vec3f(rect.min.x, rect.max.y, z), vec2f(0, 0), texture),
            VertexSpec(vec3f(rect.min.x, rect.min.y, z), vec2f(0, 1), texture),
            VertexSpec(vec3f(rect.max.x, rect.min.y, z), vec2f(1, 1), texture),
            VertexSpec(vec3f(rect.min.x, rect.max.y, z), vec2f(0, 0), texture),
            VertexSpec(vec3f(rect.max.x, rect.min.y, z), vec2f(1, 1), texture),
            VertexSpec(vec3f(rect.max.x, rect.max.y, z), vec2f(1, 0), texture),
        ];

        glUseProgram(shader.id);

        glUniformMatrix4fv(shader.uniform("mvp"), 1, true, mvp.ptr);
        glUniform4f(shader.uniform("multColor"), 1, 1, 1, 1);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture.id);
        glUniform1i(shader.uniform("texture"), 0);

        foreach (attr; VertexSpec.ATTR_NAMES)
        {
            glEnableVertexAttribArray(shader.attr(attr));
        }

        static foreach (attr; VertexSpec.ATTR_NAMES)
        {
            glVertexAttribPointer(
                shader.attr(attr),
                mixin("VertexSpec." ~ attr ~ ".sizeof") / float.sizeof,
                GL_FLOAT,
                GL_FALSE,
                VertexSpec.sizeof,
                cast(void*)(mixin("VertexSpec." ~ attr ~ ".offsetof") + cast(void*)(vertices.ptr))
            );
        }

        glDrawArrays(GL_TRIANGLES, 0, vertices.length.to!int);


        // Now draw text

        float* text_color = !enabled ? TEXT_DISABLED_COLOR.ptr :
                            hovering ? TEXT_HOVER_COLOR.ptr :
                            TEXT_COLOR.ptr;

        glUniform4fv(shader.uniform("multColor"), 1, text_color);

        size_t numVerticesAdded = get_text_mesh(label, font, text_vertices, 0, 2, z + 2);
        text_vertices.center_align(0, numVerticesAdded, rect.to!box2f);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, font.glID);
        glUniform1i(shader.uniform("texture"), 0);

        static foreach (attr; VertexSpec.ATTR_NAMES)
        {
            glVertexAttribPointer(
                shader.attr(attr),
                mixin("VertexSpec." ~ attr ~ ".sizeof") / float.sizeof,
                GL_FLOAT,
                GL_FALSE,
                VertexSpec.sizeof,
                cast(void*)(mixin("VertexSpec." ~ attr ~ ".offsetof") + cast(void*)(text_vertices.ptr))
            );
        }

        glDrawArrays(GL_TRIANGLES, 0, text_vertices.length.to!int);


        // Now draw text shadow:

        float* text_shadow_color = !enabled ? TEXT_DISABLED_SHADOW_COLOR.ptr :
                                   hovering ? TEXT_HOVER_SHADOW_COLOR.ptr :
                                   TEXT_SHADOW_COLOR.ptr;

        glUniform4fv(shader.uniform("multColor"), 1, text_shadow_color);

        if (text_shadow_vertices is null) text_shadow_vertices.length = text_vertices.length;
        text_shadow_vertices[0..$] = text_vertices[0..$];
        foreach (ref vtx; text_shadow_vertices)
        {
            vtx.position.x += 2;
            vtx.position.y -= 2;
            vtx.position.z -= 1;
        }

        static foreach (attr; VertexSpec.ATTR_NAMES)
        {
            glVertexAttribPointer(
                shader.attr(attr),
                mixin("VertexSpec." ~ attr ~ ".sizeof") / float.sizeof,
                GL_FLOAT,
                GL_FALSE,
                VertexSpec.sizeof,
                cast(void*)(mixin("VertexSpec." ~ attr ~ ".offsetof") + cast(void*)(text_shadow_vertices.ptr))
            );
        }

        glDrawArrays(GL_TRIANGLES, 0, text_shadow_vertices.length.to!int);

        foreach (attr; VertexSpec.ATTR_NAMES)
        {
            glDisableVertexAttribArray(shader.attr(attr));
        }
    }
}

/// Creates a GuiButton using the same resources (textures, shader, and font)
/// used by all menu buttons, with an empty toolip, and at z = 0 by default.
public GuiButton createMenuButton(
    Resources res,
    string label,
    void delegate() on_click,
    bool enabled = true,
    int z = 0,
)
{
    return new GuiButton(
        box2i(vec2i(0, 0), vec2i(400, 40)),
        z,
        res.textures.gui.menuButtonDefault,
        res.textures.gui.menuButtonHover,
        res.textures.gui.menuButtonDisabled,
        label,
        "",
        enabled,
        res.shaders["cube"],
        res.font,
        on_click,
    );
}
