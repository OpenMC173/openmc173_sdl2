module omc173.gui.element;

import omc173.theusual;

abstract class GuiElement
{
    public box2i rect;
    public int z;

    this(box2i rect, int z)
    {
        this.rect = rect;
        this.z = z;
    }

    public abstract void render(mat4f mvp);
}
