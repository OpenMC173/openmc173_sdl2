module omc173.gui.container;

import omc173.theusual;

import omc173.gui;

class GuiContainer : GuiElement
{
    GuiElement[] children;

    this(box2i rect, int z)
    {
        super(rect, z);
    }

    public override void render(mat4f mvp)
    {
        foreach (child; children)
        {
            child.render(mvp);
        }
    }
}
