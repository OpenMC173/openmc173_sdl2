module omc173.gui.text;

import omc173.theusual;

import omc173.graphics.vertex;


/// Renders `text` using `font` and places the vertices in the `dest` mesh.
/// The vertices are formatted so that they will be drawn properly using
/// GL_TRIANGLES. Returns the total number of vertices that were added to the
/// mesh. This is not necessarily equal to `text.length * 6` - for example,
/// `text` may contain unsupported characters, or multiple code points that
/// are represented using a single character from the bitmap font.
///
/// If the given array is too small, it will be resized by this function.
///
/// Params:
///
/// start_idx = an offset into `dest` into which the text should be placed.
///             Since the destination array is made for GL_TRIANGLES, the
///             start_idx should be some multiple of 3.
///
/// scaleFactor = indicates by how much to increase the size of the rendered
///               output relative to the size of the bitmap font (eg if the font
///               letters are 8 pixels tall, and `scaleFactor` is 2, then the
///               resulting mesh will have triangles 16 pixels tall).
///
/// `z` is the z-coordinate at which the text will be drawn.
///
/// `bottom_left` is the offset of the generated mesh from `0,0`.
public size_t get_text_mesh(
    string text,
    Font font,
    ref VertexSpec[] dest,
    size_t start_idx = 0,
    int scaleFactor = 1,
    int z = 0,
    vec2i bottom_left = vec2i(0, 0)
)
in(start_idx % 3 == 0)
{
    // Ensure that the given array is large enough:
    if (dest.length < text.length * 6 + start_idx)
    {
        dest.length = text.length * 6 + start_idx;
    }

    vec3f current_char_bottom_left = vec3f(bottom_left.x, bottom_left.y, z);
    float charHeight = font.cellSize.y * scaleFactor;

    size_t curr_idx = start_idx;

    // This iterates over code points. The UTF8 string will be iterated using
    // UTF-32 code points. I don't know if there's a better way of doing this.
    // Ideally, this should iterate over graphemes. See:
    // https://dlang.org/phobos/std_uni.html#byGrapheme
    // But `font.charInfo` should take Graphemes as the key value then? Not sure.
    foreach (dchar c; text)
    {
        if (c !in font.charInfo)
        {
            continue;
        }

        CharInfo info = font.charInfo[c];
        vec2f texSrcPos = (info.cellPos * font.cellSize) / font.imageSize.to!vec2f;
        vec2f texSrcSize = vec2f(info.width, font.cellSize.y) / font.imageSize.to!vec2f;

        dest[curr_idx .. curr_idx + 6] = [
            VertexSpec(
                vec3f(
                    current_char_bottom_left.x,
                    current_char_bottom_left.y + charHeight,
                    z
                ),
                vec2f(0, 0),
                texSrcPos,
                texSrcSize
            ),
            VertexSpec(
                vec3f(
                    current_char_bottom_left.x,
                    current_char_bottom_left.y,
                    z
                ),
                vec2f(0, 1),
                texSrcPos,
                texSrcSize
            ),
            VertexSpec(
                vec3f(
                    current_char_bottom_left.x + (info.width * scaleFactor),
                    current_char_bottom_left.y,
                    z
                ),
                vec2f(1, 1),
                texSrcPos,
                texSrcSize
            ),
            VertexSpec(
                vec3f(
                    current_char_bottom_left.x,
                    current_char_bottom_left.y + charHeight,
                    z
                ),
                vec2f(0, 0),
                texSrcPos,
                texSrcSize
            ),
            VertexSpec(
                vec3f(
                    current_char_bottom_left.x + (info.width * scaleFactor),
                    current_char_bottom_left.y,
                    z
                ),
                vec2f(1, 1),
                texSrcPos,
                texSrcSize
            ),
            VertexSpec(
                vec3f(
                    current_char_bottom_left.x + (info.width * scaleFactor),
                    current_char_bottom_left.y + charHeight,
                    z
                ),
                vec2f(1, 0),
                texSrcPos,
                texSrcSize
            ),
        ];

        current_char_bottom_left.x += (info.width + font.spacing) * scaleFactor;

        curr_idx += 6;
    }

    assert(curr_idx > start_idx);
    return curr_idx - start_idx;
}

// start is inclusive, end is exclusive
public void center_align(VertexSpec[] text_mesh, size_t start, size_t end, box2f within)
in (start % 3 == 0)
in (end % 3 == 0)
{
    float old_left = text_mesh[start].position.x;
    float text_width = text_mesh[end - 1].position.x - old_left;
    float new_left = ((within.max.x + within.min.x) / 2) - (text_width / 2);
    float translation_x = new_left - old_left;

    float old_bottom = text_mesh[start].position.y;
    float text_height = text_mesh[start + 1].position.y - old_bottom;
    float new_bottom = ((within.max.y + within.min.y) / 2) - (text_height / 2);
    float translation_y = new_bottom - old_bottom;

    foreach (ref vtx; text_mesh[start .. end])
    {
        vtx.position.x += translation_x;
        vtx.position.y += translation_y;
    }
}
