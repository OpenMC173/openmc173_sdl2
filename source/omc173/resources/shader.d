module omc173.resources.shader;

import omc173.theusual;
import std.file;
import std.path;


public class Shaders
{
    // private Shader[string] shaders;
    public Shader cube;
    public Shader twoD;

    this(string shaderDir)
    {
        Log.i("Loading shader sources...");
        // UncompiledShader[string] uncompiledShaders = loadShadersSources(shaderDir);
        Log.i("Compiling shaders...");

        // foreach (entry; uncompiledShaders.byKeyValue)
        // {
        //     if (entry.value.fsSource == string.init)
        //     {
        //         Log.w("Vertex shader \"%s.vert\" has no corresponding fragment shader.".format(entry.key));
        //         continue;
        //     }

        //     shaders[entry.key] = new Shader(entry.key, entry.value.vsSource, entry.value.fsSource);
        // }

        string cubeSourceVs = readText(buildPath(shaderDir, "cube.vert"));
        string cubeSourceFs = readText(buildPath(shaderDir, "cube.frag"));
        string twoDSourceVs = readText(buildPath(shaderDir, "2D.vert"));
        string twoDSourceFs = readText(buildPath(shaderDir, "2D.frag"));

        cube = new Shader("cube", cubeSourceVs, cubeSourceFs);
        twoD = new Shader("2D", twoDSourceVs, twoDSourceFs);
    }

    public Shader opIndex(string key)
    {
        if (key == "cube") return cube;
        if (key == "2D") return twoD;
        else throw new Exception("Cannot find shader: " ~ key);
        // if (key !in shaders)
        // {
        //     Log.e("Unable to find shader named \"" ~ key ~ "\".");
        //     throw new Exception("Unable to find shader named \"" ~ key ~ "\".");
        // }

        // return shaders[key];
    }
}

/**
 * Bundles together the various GL handles associated with a single compiled
 * shader program.
 *
 * The `id` field, which is also the constructor's only parameter, is the value
 * returned by `glCreateProgram`.
 *
 * Note that none of the methods in this struct change the state of the shader
 * program, or the state of anything else in OpenGL. Its only purpose is to
 * bundle data together and provide quick access to attribute locations, etc.
 *
 * It is assumed that the given program has been compiled and linked.
 */
public class Shader
{
    private immutable(char*) vsSourceZ;
    private immutable(char*) fsSourceZ;
    private immutable uint vs;
    private immutable uint fs;
    private uint programId;
    private int[string] attributes;
    private int[string] uniforms;

    this(string name, string vsSource, string fsSource)
    {
        this.vsSourceZ = vsSource.toStringz;
        this.fsSourceZ = fsSource.toStringz;

        this.vs = glCreateShader(GL_VERTEX_SHADER);
        compileShader(vs, name ~ ".vert", vsSourceZ);

        this.fs = glCreateShader(GL_FRAGMENT_SHADER);
        compileShader(fs, name ~ ".frag", fsSourceZ);

        programId = glCreateProgram();
        if (programId == 0)
        {
            Log.e("An error occurred when creating the GL program.");
            printGlCompileError(programId);
            throw new Exception("An error occurred when creating the GL program.");
        }

        Log.d("Created GL program for \"" ~ name ~ "\" with ID " ~ programId.to!string);
        glAttachShader(programId, vs);
        glAttachShader(programId, fs);
        glLinkProgram(programId);

        Log.d((cast(void*)vsSource).to!string);
        Log.d((cast(void*)fsSource).to!string);

        int success;
        // glGetProgramiv(programId, GL_LINK_STATUS, &success);
        // if (success == GL_FALSE)
        // {
        //     Log.e("Error linking shader.");
        //     printGlCompileError(programId);
        //     throw new Exception("Error linking shader.");
        // }
    }

    /**
     * Get the location of the attribute with the given name.
     *
     * Calls `glGetAttribLocation` and caches the result.
     */
    public int attr(string name)
    {
        if (name in attributes)
        {
            return attributes[name];
        }
        else
        {
            int attrId = glGetAttribLocation(programId, name.toStringz());
            attributes[name] = attrId;
            return attrId;
        }
    }

    /**
     * Get the location of the uniform with the given name.
     *
     * Calls `glGetUniformLocation` and caches the result.
     */
    public int uniform(string name)
    {
        if (name in uniforms)
        {
            return uniforms[name];
        }
        else
        {
            int uniformId = glGetUniformLocation(programId, name.toStringz());
            uniforms[name] = uniformId;
            return uniformId;
        }
    }

    /// Returns the GL program ID of this shader.
    public uint id()
    {
        return programId;
    }
}

private struct UncompiledShader
{
    string vsSource;
    string fsSource;
}

private UncompiledShader[string] loadShadersSources(string dir)
out(result)
{
    foreach (entry; result.byValue)
    {
        assert(entry.vsSource != string.init);
        assert(entry.fsSource != string.init);
    }
}
do
{

    import std.array : split;

    UncompiledShader[string] uncompiledShaders;

    auto vsFiles = dirEntries(dir, "*.vert", SpanMode.breadth, false);

    foreach (DirEntry file; vsFiles)
    {
        if (!file.isFile()) continue;

        string[] nameParts = file.name().baseName().stripExtension().split('.');
        string name = nameParts[0];
        uncompiledShaders[name] = UncompiledShader(readText(file.name()));
    }

    auto fsFiles = dirEntries(dir, "*.frag", SpanMode.breadth, false);

    foreach (DirEntry file; fsFiles)
    {
        if (!file.isFile()) continue;

        string[] nameParts = file.name().baseName().stripExtension().split('.');
        string name = nameParts[0];

        if (name in uncompiledShaders)
        {
            uncompiledShaders[name].fsSource = readText(file.name());
        }
        else
        {
            Log.w("Fragment shader \"" ~ file.name.baseName ~ "\" has no corresponding vertex shader.");
        }
    }

    string[] toRemove;

    foreach (entry; uncompiledShaders.byKeyValue)
    {
        if (entry.value.fsSource == string.init)
        {
            Log.w("Vertex shader \"" ~ entry.key ~ "%s.vert\" has no corresponding fragment shader.", entry.key);
            toRemove ~= entry.key;
        }
    }

    foreach (key; toRemove)
    {
        uncompiledShaders.remove(key);
    }

    return uncompiledShaders;
}



/**
 * Compile the shader with the given ID and source code.
 *
 * Note that `source` should contain the actual source code as a string,
 * not a path to a file containing the source code.
 *
 * If the shader fails to compile, the compilation log will be printed and an
 * Exception will be thrown.
 */
private void compileShader(uint shader, string filename, const(char)* sourceZ)
{
    Log.d("Compiling " ~ filename ~ "...");
    glShaderSource(shader, 1, &sourceZ, null);
    glCompileShader(shader);

    int success;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (success == GL_FALSE)
    {
        Log.e("ERROR: Error compiling shader `" ~ filename ~ "`:");
        printGlCompileError(shader);
        throw new Exception("Error compiling shader.`" ~ filename ~ "`.");
    }
}

private void printGlCompileError(int BUFFER_MAX_LENGTH = 4096)(uint shaderOrProgram)
{
    typeof(glGetShaderInfoLog) getInfoLog;
    if (glIsShader(shaderOrProgram)) getInfoLog = glGetShaderInfoLog;
    else if (glIsProgram(shaderOrProgram)) getInfoLog = glGetProgramInfoLog;
    else
    {
        Log.e("Given handle is neither a shader or a program: %d".format(shaderOrProgram));
        throw new Exception("Given handle is neither a shader or a program.");
    }

    char[BUFFER_MAX_LENGTH] buffer;
    int length;

    getInfoLog(shaderOrProgram, BUFFER_MAX_LENGTH, &length, &buffer[0]);
    Log.e(buffer[0 .. length].to!string);
}
