/**
 * Minecraft's original texture pack structure is as follows:
 *
 * - root folder
 *     - achievement    GUI of the Achievements screen.
 *     - armor          Images drawn on a second layer of character/mob models,
 *                      including armor and power-creeper's "electric" overlay.
 *                      Does not include spider eyes or pig saddles, those are
 *                      in `/mob/`.
 *     - art            Paintings.
 *     - environment    Clouds, rain, and snow.
 *     - font           In-game typeface, in PNG format.
 *     - gui            GUIs of blocks (furnaces etc.), HUD images, main menu
 *                      images, and item icons.
 *     - item           Non-mob entities: Boats, Arrows, Minecarts, Signs
 *     - misc           Biome colors, item shadow, as well as various GUI
 *                      images: map (when held in hand) and its player icons,
 *                      water overlay (when diving), pumpkin HUD overlay,
 *                      vignette overlay, clock dial.
 *     - mob            Player and mob skins.
 *     - terrain        Sun and Moon images, for some reason.
 *     - title          The "new" Minecraft logo (as of beta 1.4), mojang logo,
 *                      splash screen text, and black.png (which may be the
 *                      overlay used to dim the main menu background?).
 *     - pack.png       Icon shown in the "Texture Packs" screen next to the
 *                      current texture pack.
 *     - particles.png  Particle effects (rain, snow, torch flame, etc.).
 *     - terrain.png    Block textures.
 *
 */
module omc173.resources.texture_packs;


import std.path : baseName, buildNormalizedPath;

import imageformats;

import omc173.theusual;


public class Textures
{
    public Blocks blocks;
    public Gui gui;

    this(string texturePackDir)
    {
        Log.i("Loading texture pack \"" ~ baseName(texturePackDir) ~ "\"...");
        loadTerrainPng(texturePackDir);
        loadGuiFolder(buildNormalizedPath(texturePackDir, "gui"));
        loadTitleFolder(buildNormalizedPath(texturePackDir, "title"));
    }

    private struct Gui
    {
        Texture menuButtonDefault;
        Texture menuButtonHover;
        Texture menuButtonDisabled;
        Texture menuBackground;
        Texture menuLogoLeft;
        Texture menuLogoRight;
    }

    private struct Blocks
    {
        Texture stone;
        Texture grassTop;
        Texture grassSide;
        Texture grassBottom;
        Texture dirt;
        Texture cobblestone;
        Texture planksOak;
        Texture saplingOak;
        Texture saplingSpruce;
        Texture saplingBirch;
        Texture bedrock;
    }

    private void loadTerrainPng(string texturePackDir)
    {
        uint terrainPngId = loadPngIntoOpenGL(buildNormalizedPath(texturePackDir, "terrain.png"));

        blocks.stone =          Texture(terrainPngId, vec2f( 1/16f,  0/16f), vec2f(1/16f, 1/16f));
        blocks.grassTop =      Texture(terrainPngId, vec2f( 0/16f,  0/16f), vec2f(1/16f, 1/16f));
        blocks.grassSide =     Texture(terrainPngId, vec2f( 3/16f,  0/16f), vec2f(1/16f, 1/16f));
        blocks.grassBottom =   Texture(terrainPngId, vec2f( 2/16f,  0/16f), vec2f(1/16f, 1/16f));
        blocks.dirt =           Texture(terrainPngId, vec2f( 2/16f,  0/16f), vec2f(1/16f, 1/16f));
        blocks.cobblestone =    Texture(terrainPngId, vec2f( 0/16f,  1/16f), vec2f(1/16f, 1/16f));
        blocks.planksOak =     Texture(terrainPngId, vec2f( 4/16f,  0/16f), vec2f(1/16f, 1/16f));
        blocks.saplingOak =    Texture(terrainPngId, vec2f(15/16f,  0/16f), vec2f(1/16f, 1/16f));
        blocks.saplingBirch =  Texture(terrainPngId, vec2f(15/16f,  3/16f), vec2f(1/16f, 1/16f));
        blocks.saplingSpruce = Texture(terrainPngId, vec2f(15/16f,  4/16f), vec2f(1/16f, 1/16f));
        blocks.bedrock =        Texture(terrainPngId, vec2f( 1/16f,  1/16f), vec2f(1/16f, 1/16f));
    }

    private void loadGuiFolder(string guiDir)
    {
        uint guiPngId = loadPngIntoOpenGL(buildNormalizedPath(guiDir, "gui.png"));

        gui.menuButtonDefault =  Texture(guiPngId, vec2f(0f, 66/256f), vec2f(200f/256f, 20f/256f));
        gui.menuButtonHover =    Texture(guiPngId, vec2f(0f, 86/256f), vec2f(200f/256f, 20f/256f));
        gui.menuButtonDisabled = Texture(guiPngId, vec2f(0f, 46/256f), vec2f(200f/256f, 20f/256f));


        uint backgroundPngId = loadPngIntoOpenGL(buildNormalizedPath(guiDir, "background.png"));

        gui.menuBackground = Texture(backgroundPngId);
    }

    private void loadTitleFolder(string titleFolder)
    {
        // mclogo.png and maybe splashes.txt are the only things we care about here.

        uint mclogoPngId = loadPngIntoOpenGL(buildNormalizedPath(titleFolder, "mclogo.png"));

        gui.menuLogoLeft = Texture(mclogoPngId, vec2f(0, 0), vec2f(155/256f, 44/256f));
        gui.menuLogoRight = Texture(mclogoPngId, vec2f(0, 45/256f), vec2f(119/256f, 44/256f));
    }
}

public struct Texture
{
    uint id = 0;
    vec2f srcPos = vec2f(0f, 0f);
    vec2f srcSize = vec2f(1f, 1f);
}

/// Loads a PNG file, calls glGenTextures, and returns the ID.
package uint loadPngIntoOpenGL(string path)
{
    IFImage image = read_png(path);

    uint glColorFormat;

    final switch (image.c)
    {
        case ColFmt.RGB:
            glColorFormat = GL_RGB;
            break;

        case ColFmt.RGBA:
            glColorFormat = GL_RGBA;
            break;

        case ColFmt.Y:
        case ColFmt.YA:
            Log.e("Unsupported color format: " ~ image.c.to!string);
            return 0;
    }

    uint id;
    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(
        GL_TEXTURE_2D,
        0,                  // Not a mipmap, i.e. level=0
        glColorFormat,      // All files have been converted to SDL's ABGR which corresponds to GL's RGBA on little-endian processors
        image.w,            // width and height
        image.h,
        0,                  // Must be 0 for whatever reason
        glColorFormat,      // Not sure of the difference between this and internalFormat
        GL_UNSIGNED_BYTE,   // Channels are composed of 8-bit integers
        image.pixels.ptr
    );

    return id;
}
