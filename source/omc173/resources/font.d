module omc173.resources.font;

import omc173.theusual;

import std.path : buildNormalizedPath;

import omc173.resources.texture_packs : loadPngIntoOpenGL;


/// Handled separately from Texture Packs. Fonts also depend on things like
/// languages, so it might be better to have them separated like this.
public class Font
{
    public uint glID;
    public vec2i cellSize;
    public CharInfo[dchar] charInfo;
    public ubyte spacing;
    public vec2i imageSize;

    this(string texturePackDir)
    {
        Log.i("Loading font...");
        glID = loadPngIntoOpenGL(buildNormalizedPath(texturePackDir, "font", "default.png"));

        // Minecraft's default font has these character positions and widths:

        cellSize = vec2i(8, 8);
        spacing = 1;
        imageSize = vec2i(128, 128);

        charInfo[' ']  = CharInfo(vec2i( 0, 2), 3);
        charInfo['!']  = CharInfo(vec2i( 1, 2), 1);
        charInfo['"']  = CharInfo(vec2i( 2, 2), 4);
        charInfo['#']  = CharInfo(vec2i( 3, 2), 5);
        charInfo['$']  = CharInfo(vec2i( 4, 2), 5);
        charInfo['%']  = CharInfo(vec2i( 5, 2), 5);
        charInfo['&']  = CharInfo(vec2i( 6, 2), 5);
        charInfo['\''] = CharInfo(vec2i( 7, 2), 2);
        charInfo['(']  = CharInfo(vec2i( 8, 2), 4);
        charInfo[')']  = CharInfo(vec2i( 9, 2), 5);
        charInfo['*']  = CharInfo(vec2i(10, 2), 4);
        charInfo['+']  = CharInfo(vec2i(11, 2), 5);
        charInfo[',']  = CharInfo(vec2i(12, 2), 1);
        charInfo['-']  = CharInfo(vec2i(13, 2), 5);
        charInfo['.']  = CharInfo(vec2i(14, 2), 1);
        charInfo['/']  = CharInfo(vec2i(15, 2), 5);

        charInfo['0']  = CharInfo(vec2i( 0, 3), 5);
        charInfo['1']  = CharInfo(vec2i( 1, 3), 5);
        charInfo['2']  = CharInfo(vec2i( 2, 3), 5);
        charInfo['3']  = CharInfo(vec2i( 3, 3), 5);
        charInfo['4']  = CharInfo(vec2i( 4, 3), 5);
        charInfo['5']  = CharInfo(vec2i( 5, 3), 5);
        charInfo['6']  = CharInfo(vec2i( 6, 3), 5);
        charInfo['7']  = CharInfo(vec2i( 7, 3), 5);
        charInfo['8']  = CharInfo(vec2i( 8, 3), 5);
        charInfo['9']  = CharInfo(vec2i( 9, 3), 5);
        charInfo[':']  = CharInfo(vec2i(10, 3), 4);
        charInfo[';']  = CharInfo(vec2i(11, 3), 5);
        charInfo['<']  = CharInfo(vec2i(12, 3), 1);
        charInfo['=']  = CharInfo(vec2i(13, 3), 5);
        charInfo['>']  = CharInfo(vec2i(14, 3), 1);
        charInfo['?']  = CharInfo(vec2i(15, 3), 5);

        charInfo['@']  = CharInfo(vec2i( 0, 4), 6);
        charInfo['A']  = CharInfo(vec2i( 1, 4), 5);
        charInfo['B']  = CharInfo(vec2i( 2, 4), 5);
        charInfo['C']  = CharInfo(vec2i( 3, 4), 5);
        charInfo['D']  = CharInfo(vec2i( 4, 4), 5);
        charInfo['E']  = CharInfo(vec2i( 5, 4), 5);
        charInfo['F']  = CharInfo(vec2i( 6, 4), 5);
        charInfo['G']  = CharInfo(vec2i( 7, 4), 5);
        charInfo['H']  = CharInfo(vec2i( 8, 4), 5);
        charInfo['I']  = CharInfo(vec2i( 9, 4), 5);
        charInfo['J']  = CharInfo(vec2i(10, 4), 5);
        charInfo['K']  = CharInfo(vec2i(11, 4), 5);
        charInfo['L']  = CharInfo(vec2i(12, 4), 5);
        charInfo['M']  = CharInfo(vec2i(13, 4), 5);
        charInfo['N']  = CharInfo(vec2i(14, 4), 5);
        charInfo['O']  = CharInfo(vec2i(15, 4), 5);

        charInfo['P']  = CharInfo(vec2i( 0, 5), 5);
        charInfo['Q']  = CharInfo(vec2i( 1, 5), 5);
        charInfo['R']  = CharInfo(vec2i( 2, 5), 5);
        charInfo['S']  = CharInfo(vec2i( 3, 5), 5);
        charInfo['T']  = CharInfo(vec2i( 4, 5), 5);
        charInfo['U']  = CharInfo(vec2i( 5, 5), 5);
        charInfo['V']  = CharInfo(vec2i( 6, 5), 5);
        charInfo['W']  = CharInfo(vec2i( 7, 5), 5);
        charInfo['X']  = CharInfo(vec2i( 8, 5), 5);
        charInfo['Y']  = CharInfo(vec2i( 9, 5), 5);
        charInfo['Z']  = CharInfo(vec2i(10, 5), 5);
        charInfo['[']  = CharInfo(vec2i(11, 5), 3);
        charInfo['\\'] = CharInfo(vec2i(12, 5), 5);
        charInfo[']']  = CharInfo(vec2i(13, 5), 3);
        charInfo['^']  = CharInfo(vec2i(14, 5), 5);
        charInfo['_']  = CharInfo(vec2i(15, 5), 5);

        charInfo['`']  = CharInfo(vec2i( 0, 6), 2);
        charInfo['a']  = CharInfo(vec2i( 1, 6), 5);
        charInfo['b']  = CharInfo(vec2i( 2, 6), 5);
        charInfo['c']  = CharInfo(vec2i( 3, 6), 5);
        charInfo['d']  = CharInfo(vec2i( 4, 6), 5);
        charInfo['e']  = CharInfo(vec2i( 5, 6), 5);
        charInfo['f']  = CharInfo(vec2i( 6, 6), 4);
        charInfo['g']  = CharInfo(vec2i( 7, 6), 5);
        charInfo['h']  = CharInfo(vec2i( 8, 6), 5);
        charInfo['i']  = CharInfo(vec2i( 9, 6), 1);
        charInfo['j']  = CharInfo(vec2i(10, 6), 5);
        charInfo['k']  = CharInfo(vec2i(11, 6), 4);
        charInfo['l']  = CharInfo(vec2i(12, 6), 2);
        charInfo['m']  = CharInfo(vec2i(13, 6), 5);
        charInfo['n']  = CharInfo(vec2i(14, 6), 5);
        charInfo['o']  = CharInfo(vec2i(15, 6), 5);

        charInfo['p']  = CharInfo(vec2i( 0, 7), 5);
        charInfo['q']  = CharInfo(vec2i( 1, 7), 5);
        charInfo['r']  = CharInfo(vec2i( 2, 7), 5);
        charInfo['s']  = CharInfo(vec2i( 3, 7), 5);
        charInfo['t']  = CharInfo(vec2i( 4, 7), 3);
        charInfo['u']  = CharInfo(vec2i( 5, 7), 5);
        charInfo['v']  = CharInfo(vec2i( 6, 7), 5);
        charInfo['w']  = CharInfo(vec2i( 7, 7), 5);
        charInfo['x']  = CharInfo(vec2i( 8, 7), 5);
        charInfo['y']  = CharInfo(vec2i( 9, 7), 5);
        charInfo['z']  = CharInfo(vec2i(10, 7), 5);
        charInfo['{']  = CharInfo(vec2i(11, 7), 4);
        charInfo['|']  = CharInfo(vec2i(12, 7), 1);
        charInfo['}']  = CharInfo(vec2i(13, 7), 4);
        charInfo['~']  = CharInfo(vec2i(14, 7), 6);

        // That's good for now. Should do the rest of the characters eventually.
        // Also, find a way to represent characters that don't have a dedicated
        // glyph (could use something that looks like '�'). Unfortunately it
        // doesn't look like the vanilla "font/default.png" font has anything
        // like that, so not sure what to do yet.
    }
}

public struct CharInfo
{
    vec2i cellPos;
    ubyte width;
}
