module omc173.resources.loader;

import std.path : buildNormalizedPath;

import omc173.resources;
import omc173.util;


/**
 * Provides access to all assets and resources required by the game.
 *
 * This includes:
 *     - Compiled shader programs.
 *     - Textures from texture packs.
 */
class Resources
{
    private enum string SHADERS_DIR = "shaders";
    private enum string TEXTURE_PACKS_DIR = "texturepacks";


    /// Map of shader names to shader IDs.
    public Shaders shaders;

    /**
     * Map of texture names to texture buffer IDs.
     *
     * Texture names correspond to file names (without extensions or folder names).
     */
    public Textures textures;

    /// Bitmap font information
    public Font font;

    private Shader cube;
    private Shader twoD;

    this()
    {
        shaders = new Shaders(SHADERS_DIR);
        // import core.thread : Thread, dur; Thread.sleep(2.dur!"seconds");
        textures = new Textures(buildNormalizedPath(TEXTURE_PACKS_DIR, "Default 16x16"));
        font = new Font(buildNormalizedPath(TEXTURE_PACKS_DIR, "Default 16x16"));
    }
}
