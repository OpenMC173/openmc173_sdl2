module omc173.logging.log;

import omc173.theusual;

import std.stdio;
import std.datetime : SysTime, Clock;

import omc173.logging.severities;
import omc173.logging.outputs.log_output;


public shared class Log
{
    private shared static Log log;

    private Severity minSeverity;
    private bool silent;
    private LogOutput[] outputs;

    private this(Severity minSeverity, bool silent, shared LogOutput[] outputs...)
    {
        this.minSeverity = minSeverity;
        this.silent = silent;
        this.outputs = outputs;
    }

    public static void setupLog(Severity minSeverity, bool silent, shared LogOutput[] outputs...)
    {
        if (log !is null)
        {
            writeln("Logger has already been setup.");
            return;
        }

        if (!silent) writeln("Creating logger with severity " ~ minSeverity.to!string ~ ".");

        log = new shared Log(minSeverity, silent, outputs);
    }

    public static void shutDownLog()
    {
        if (log is null)
        {
            return;
        }

        foreach (output; log.outputs)
        {
            output.shutDown();
        }
    }

    public static void d(string message, string modul = __MODULE__)
    {
        if (log.minSeverity <= Severity.DEBUG)
        {
            commonLog(modul, Severity.DEBUG, message);
        }
    }

    public static void i(string message, string modul = __MODULE__)
    {
        if (log.minSeverity <= Severity.INFO)
        {
            commonLog(modul, Severity.INFO, message);
        }
    }

    public static void w(string message, string modul = __MODULE__)
    {
        if (log.minSeverity <= Severity.WARNING)
        {
            commonLog(modul, Severity.WARNING, message);
        }
    }

    public static void e(string message, string modul = __MODULE__)
    {
        if (log.minSeverity <= Severity.ERROR)
        {
            commonLog(modul, Severity.ERROR, message);
        }
    }

    private static void commonLog(string modul, Severity severity, string message)
    {
        synchronized (log)
        {
            if (log.silent) return;

            if (log is null)
            {
                writeln("Logger not yet set up!");
                return;
            }

            import std.datetime : Clock;

            SysTime currTime = Clock.currTime();

            foreach (output; log.outputs)
            {
                import std.string : splitLines;
                import std.algorithm : filter;
                import std.range : empty;

                bool printingFirst = true;
                foreach (string line; splitLines(message).filter!(e => !e.empty))
                {
                    output.log(currTime, modul, severity, line, !printingFirst);
                    printingFirst = false;
                }
            }
        }
    }
}
