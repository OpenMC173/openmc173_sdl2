module omc173.logging.severities;

import omc173.theusual;

import std.uni : toLower;


public enum Severity
{
    DEBUG,
    INFO,
    WARNING,
    ERROR,
}

public Severity fromString(string str)
{
    switch (str.toLower())
    {
        case "debug":
            return Severity.DEBUG;
        case "info":
            return Severity.INFO;
        case "warning":
            return Severity.WARNING;
        case "error":
            return Severity.ERROR;
        default:
            throw new Exception("Unrecognized severity: " ~ str);
    }
}

unittest
{
    writeln("TEST: fromString()");

    assert(fromString("debug") == Severity.DEBUG);
    assert(fromString("DEBUG") == Severity.DEBUG);
    assert(fromString("dEbuG") == Severity.DEBUG);
    assert(fromString("info") == Severity.INFO);
    assert(fromString("warning") == Severity.WARNING);
    assert(fromString("error") == Severity.ERROR);

    try
    {
        fromString("notadebuglevel");
        assert(false, "Expected `fromString(\"notadebuglevel\")` to throw an error.");
    }
    catch (Exception e)
    {
        // Good.
    }
}
