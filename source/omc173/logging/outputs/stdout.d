module omc173.logging.outputs.stdout;

import omc173.theusual;

import std.stdio : write, writeln, writefln;
import std.datetime : SysTime;
import std.range : walkLength, padLeft;
import std.uni : byGrapheme;
import consoled;
import omc173.logging.outputs.log_output;
import omc173.logging.severities : Severity;

final shared class StdoutOutput : LogOutput
{
    private void function(SysTime, string, Severity, string, bool) logFunc;
    private size_t modulColWidth = 30;

    this(bool noColors)
    {
        logFunc = noColors ? &logMonochrome : &logColored;
    }

    public override void log(SysTime timestamp, string modul, Severity severity, string message, bool continuation)
    {
        logFunc(timestamp, modul, severity, message, continuation);
    }

    private static void logMonochrome(SysTime timestamp, string modul, Severity severity, string message, bool continuation)
    {
        string time = formatTime(timestamp, "%T.%g", false);
        writeln("%s | %s | %s | %s".format(
            continuation ? `"`.padLeft(' ', time.byGrapheme.walkLength).to!string : time,
            modul,
            severity.to!string,
            message
        ));
    }

    private static void logColored(SysTime timestamp, string modul, Severity severity, string message, bool continuation)
    {
        // Without this, everything is double-underlined for me on Linux...?
        fontStyle = FontStyle.none;
        resetColors();

        // Timestamp

        string time = formatTime(timestamp, "%T.%g", false);

        foreground = Color.gray;
        write(continuation ? `"`.padLeft(' ', time.byGrapheme.walkLength).to!string : time);
        resetColors();
        write(' ');


        // Severity

        dchar severitySymbol;
        final switch (severity)
        {
            // I'd like to use emoji here, but they don't seem to work on Windows... sad.
            case Severity.ERROR:
                fontStyle = FontStyle.bold;
                foreground = Color.white;
                background = Color.lightRed;
                severitySymbol = 'E';
                break;

            case Severity.WARNING:
                fontStyle = FontStyle.bold;
                foreground = Color.black;
                background = Color.lightYellow;
                severitySymbol = 'W';
                break;

            case Severity.INFO:
                foreground = Color.lightGreen;
                severitySymbol = 'i';
                break;

            case Severity.DEBUG:
                foreground = Color.lightCyan;
                severitySymbol = 'd';
                break;
        }

        write(' ', severitySymbol, ' ');
        resetColors();
        resetFontStyle();
        write(' ');


        // Module

        foreground = Color.magenta;
        write(modul);
        resetColors();

        // size_t modul_length = modul.byGrapheme.walkLength;
        // foreach (i; 0 .. modulColWidth - modul_length)
        // {
        //     write(' ');
        // }

        write(' ');


        // Message

        writeln(message);
    }

    public override void shutDown()
    {
        // Nothing to do here.
    }
}
