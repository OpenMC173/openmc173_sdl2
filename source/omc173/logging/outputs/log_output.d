module omc173.logging.outputs.log_output;

import std.datetime : SysTime;

import omc173.logging.severities;

/// Each implementation of this class handles the printing to a specific
/// output medium (such as standard output, a file output, a custom console
/// window, a null output, etc.).
shared interface LogOutput
{
    public void log(SysTime timestamp, string module_, Severity severity, string message, bool continuation);

    public void shutDown();
}
