module omc173.logging.outputs.simple_file;

import omc173.theusual;

import core.atomic;

import std.datetime : Clock, Duration, dur;
import std.datetime.systime;
import std.path;
import std.file;

import omc173.logging.outputs.log_output;
import omc173.logging.severities : Severity;


shared class SimpleFileOutput : LogOutput
{
    private string buffer;

    /// Number of lines in the current buffer.
    private size_t currentBatchSize = 0;

    /// The current buffer needs to be at least this many lines long before
    /// being written to the file.
    private size_t minimumBatchSize;

    private string outputDir;
    private string currOutputFile;
    invariant(currOutputFile == "" || currOutputFile.isValidPath(), "Invalid file path: " ~ currOutputFile);
    invariant(outputDir.isValidPath(), "Invalid output directory: " ~ outputDir);

    /// At least this much time needs to pass before the output file may be
    /// written to once more.
    private Duration saveInterval;
    private SysTime prevSaveTime;

    this(string outputDir, size_t minimumBatchSize, Duration saveInterval = dur!"seconds"(60))
    in (isValidPath(outputDir))
    {
        this.outputDir = outputDir;

        if (!exists(outputDir))
        {
            mkdirRecurse(outputDir);
        }

        this.minimumBatchSize = minimumBatchSize;
        this.saveInterval = saveInterval;
        prevSaveTime = Clock.currTime();
    }

    public override void log(SysTime timestamp, string module_, Severity severity, string message, bool continuation)
    {
        import std.range : padLeft, walkLength;
        import std.uni : byGrapheme;
        string time = formatTime(timestamp, "%F %T.%g", true);
        buffer ~= "%s | %s | %s | %s\n".format(
            continuation ? `"`.padLeft(' ', time.byGrapheme.walkLength).to!string : time,
            module_,
            severity.to!string,
            message
        );

        currentBatchSize.atomicOp!"+="(1);

        if (currentBatchSize >= minimumBatchSize &&
            (Clock.currTime() - prevSaveTime) > saveInterval)
        {
            writeToFile();
        }
    }

    private void writeToFile()
    {
        // import std.string : ;

        if (currOutputFile == "")
        {
            currOutputFile = buildNormalizedPath(outputDir, getTimestampForPath("_") ~ ".log");
            assert(currOutputFile.isValidPath(), "Invalid file path: " ~ currOutputFile);
        }

        append(currOutputFile, buffer);

        buffer = "";
        currentBatchSize = 0;
        prevSaveTime.atomicStore(Clock.currTime().to!(shared(SysTime)));
    }

    public override void shutDown()
    {
        writeToFile();
    }
}
