module omc173.debugging;

import omc173.theusual;
import omc173.main : requestQuit;

import std.traits;


static if (useKHRDebug)
{
    /// Callback function passed to `glDebugMessageCallback`. Not meant to be called
    /// directly. Complains if extern(System) isn't there, I just did what the
    /// `GLDEBUGPROC` alias definition does in core_43.d
    extern(System) void logGlMessage(
        GLenum source,
        GLenum type,
        GLuint id,
        GLenum severity,
        GLsizei length,
        const(char)* message,
        void* userParam
    ) nothrow
    {
        try
        {
            if (severity != GL_DEBUG_SEVERITY_NOTIFICATION) requestQuit();
            Log.i(
                "Received message from OpenGL:\n" ~
                "\tsource:     %s\n".format(glSourceToString(source)) ~
                "\ttype:       %s\n".format(glTypeToString(type)) ~
                "\tid:         0x%s\n".format(id.to!string(16)) ~
                "\tseverity:   %s\n".format(glSeverityToString(severity)) ~
                "\tmessage:    %s\n".format(message[0..length])
            );
            if (severity != GL_DEBUG_SEVERITY_NOTIFICATION) Log.w("Program will quit after current game loop.");
        }
        catch (Exception e)
        {
            // Nothing we can do. STDOUT functions aren't nothrow, so we can't
            // even log a warning or anything like that. Just quit and hope for
            // the best.
            // Exit code is 173 because OpenMC173

            import core.stdc.stdlib : exit;
            exit(173);
        }
    }

    private string glSeverityToString(GLenum severity)
    {
        switch (severity)
        {
            case GL_DEBUG_SEVERITY_HIGH:            return "HIGH";
            case GL_DEBUG_SEVERITY_MEDIUM:          return "MEDIUM";
            case GL_DEBUG_SEVERITY_LOW:             return "LOW";
            case GL_DEBUG_SEVERITY_NOTIFICATION:    return "NOTIFICATION";
            default:                                return "Unknown: 0x" ~ severity.to!string(16);
        }
    }

    private string glTypeToString(GLenum type)
    {
        switch (type)
        {
            case GL_DEBUG_TYPE_ERROR:               return "ERROR";
            case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: return "DEPRECATED BEHAVIOR";
            case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  return "UNDEFINED BEHAVIOR";
            case GL_DEBUG_TYPE_PORTABILITY:         return "PORTABILITY";
            case GL_DEBUG_TYPE_PERFORMANCE:         return "PERFORMANCE";
            case GL_DEBUG_TYPE_OTHER:               return "OTHER";
            case GL_DEBUG_TYPE_MARKER:              return "MARKER ?";
            case GL_DEBUG_TYPE_PUSH_GROUP:          return "PUSH GROUP ?";
            case GL_DEBUG_TYPE_POP_GROUP:           return "POP GROUP ?";
            default:                                return "Unknown: 0x" ~ type.to!string(16);
        }
    }

    private string glSourceToString(GLenum source)
    {
        switch (source)
        {
            case GL_DEBUG_SOURCE_API:               return "API";
            case GL_DEBUG_SOURCE_WINDOW_SYSTEM:     return "WINDOW SYSTEM";
            case GL_DEBUG_SOURCE_SHADER_COMPILER:   return "SHADER COMPILER";
            case GL_DEBUG_SOURCE_THIRD_PARTY:       return "THIRD PARTY";
            case GL_DEBUG_SOURCE_APPLICATION:       return "APPLICATION";
            case GL_DEBUG_SOURCE_OTHER:             return "OTHER";
            default:                                return "Unknown: 0x" ~ source.to!string(16);
        }
    }
}

/**
 * Should SDL return an error code from one of its functions, this function may
 * be used to retrieve an error message.
 */
public void printSdlError(string prelude, string module_ = __MODULE__)
{
    import std.conv : to;

    string errorMsg = SDL_GetError().to!string;
    Log.e((prelude != "" ? prelude : "Error in SDL:") ~ "\n" ~ errorMsg, module_);
    SDL_ClearError();
}

/**
 * Helper function to determine if the previous SDL call caused an error.
 * Used when only one possible value is not an error.
 *
 *     SDL_SomeRandomFunction().chk;
 *
 * is equivalent to:
 *
 *     if (!SDL_SomeRandomFunction() != 0)
 *     {
 *         printSdlError();
 *         requestQuit();
 *     }
 */
public void chk(int ALLOWED = 0)(int result, string module_ = __MODULE__, string file = __FILE__, int line = __LINE__)
{
    if (result != ALLOWED)
    {
        printSdlError("Received error code " ~ result ~ " from SDL in \"" ~ file ~ "\" at line " ~ line.to!string, module_);
        requestQuit();
    }
}

/**
 * Helper function to determine if the previous SDL call caused an error.
 * Used when only one possible value (e.g., -1) indicates an error.
 *
 *     SDL_SomeRandomFunction().chkX;
 *
 * is equivalent to:
 *
 *     if (!SDL_SomeRandomFunction() == -1)
 *     {
 *         printSdlError();
 *         requestQuit();
 *     }
 */
public void chkX(int FORBIDDEN = -1)(int result, string module_ = __MODULE__, string file = __FILE__, int line = __LINE__)
{
    if (result == FORBIDDEN)
    {
        printSdlError("Received error code " ~ FORBIDDEN.to!string ~ " from SDL in \"" ~ file ~ "\" at line " ~ line.to!string, module_);
        requestQuit();
    }
}

/// Helper function to determine if the previous SDL call caused an error.
/// Used when a null pointer indicates an error. If not null, returns the given
/// pointer.
///
/// ```d
///     auto x = SDL_SomeRandomFunction().chkNull;
/// ```
///
/// is equivalent to:
///
/// ```d
///     auto x = SDL_SomeRandomFunction();
///
///     if (x == null)
///     {
///         printSdlError();
///         requestQuit();
///     }
/// ```
///
public T chkNull(T)(T result, string module_ = __MODULE__, string file = __FILE__, int line = __LINE__)
if (isPointer!T)
{
    if (result == null)
    {
        printSdlError("Received null pointer from SDL in \"" ~ file ~ "\" at line " ~ line.to!string, module_);
        requestQuit();
    }

    return result;
}


/// Returns the number of SDL performance counter values that have "elapsed"
/// since this function was last called. Useful for profiling.
public ulong lap()
{
    static ulong lastCount = 0;
    ulong currentCount = SDL_GetPerformanceCounter();
    ulong diff = currentCount - lastCount;
    lastCount = currentCount;
    return diff;
}
