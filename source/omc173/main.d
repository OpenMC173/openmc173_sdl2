module omc173.main;

import std.conv : to;
import std.format;
import std.getopt;

import bindbc.sdl;
import bindbc.sdl.bind;
import bindbc.opengl;
import bindbc.opengl.bind;

import gfm.math;

import omc173.debugging;
import omc173.game.game_screen;
import omc173.logging;
import omc173.logging.severities;
import omc173.logging.outputs;
import omc173.mainmenu.mainmenuscreen;
import omc173.resources;
import omc173.screen;


private SDL_Window* window;

public SDL_Window* getSdlWindow()
{
    return window;
}

version(unittest)
{
    private void main()
    {
        import std.stdio : writeln;
        writeln("Ran all unit tests, quitting.");
    }
}
else
{

private void main(string[] args)
{
    /***********************************************
     *                                             *
     *         Parse Command Line Options          *
     *                                             *
     ***********************************************/

    string logMinSeverity = "";
    bool logNoColors = false;
    bool verbose = false;
    bool silent = false;

    auto opt = getopt(
        args,

        "log-min-severity",
        "Only messages with at least this severity will be logged. <debug|info|warning|error>.",
        &logMinSeverity,

        "verbose",
        "Equivalent to `--log-min-severity=DEBUG`. Overrides `--log-min-severity`.",
        &verbose,

        "silent",
        "Disables logger. Overrides `--log-min-severity` and `--verbose`.",
        &silent,

        "log-no-colors",
        "Disables colors in log output.",
        &logNoColors
    );

    if (opt.helpWanted)
    {
        defaultGetoptPrinter("OpenMC173 provides the following optional command-line parameters:\n", opt.options);
        return;
    }

    Severity severity = Severity.INFO;
    debug { severity = Severity.DEBUG; }
    severity = verbose ? Severity.DEBUG :
               logMinSeverity != "" ? fromString(logMinSeverity) :
               severity;



    /***********************************************
     *                                             *
     *                    Setup                    *
     *                                             *
     ***********************************************/


    // Initialize logger:

    Log.setupLog(severity, silent, new shared StdoutOutput(logNoColors), new shared SimpleFileOutput("./log", 100));


    // Load SDL2

    Log.i("Loading SDL2...");

    SDLSupport loadedSdlVersion = loadSDL();
    if (loadedSdlVersion != sdlSupport)
    {
        Log.e("Failed to load SDL2 dynamic library.");
        return exit();
    }

    SDLImageSupport loadedSdlImageVersion = loadSDLImage();
    if (loadedSdlImageVersion != sdlImageSupport)
    {
        Log.e("Failed to load SDL_image.");
        return exit();
    }

    // TODO: Calling SDL_Init with SDL_INIT_AUDIO crashes despite this succeeding.
    SDLMixerSupport loadedSdlMixerVersion = loadSDLMixer();
    if (loadedSdlMixerVersion != sdlMixerSupport)
    {
        Log.e("Failed to load SDL_mixer. Expected " ~ sdlMixerSupport.to!string ~ ", received " ~ loadedSdlMixerVersion.to!string ~ ".");
        return exit();
    }


    // Initialize SDL2
    Log.i("Initializing SDL2...");

    // If this is called using SDL_INIT_EVERYTHING, it crashes only under the
    // following very specific scenario:
    // Running the program from within Visual Studio Code on Linux (even when
    // run manually from the integrated terminal).
    // Probably there's something weird going on with how the integrated
    // terminal handles dynamic libraries that can't be found...? But it seems
    // that explicitly specifying the subsystems we need fixes it.
    // The real mystery is, why doesn't it crash on Windows or when run from an
    // external terminal???
    int status = SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO | SDL_INIT_EVENTS/*  | SDL_INIT_AUDIO */);
    if (status != 0)
    {
        printSdlError("Failed to initialize SDL2.");
        return exit();
    }


    // Create window
    Log.i("Creating window...");
    window = SDL_CreateWindow(
        "OpenMC173",
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        800,
        600,
        SDL_WINDOW_OPENGL | SDL_WINDOW_MAXIMIZED | SDL_WINDOW_RESIZABLE
    ).chkNull;

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, SDL_TRUE);

    // Create rendering context
    Log.i("Creating rendering context...");
    SDL_GL_CreateContext(window).chkNull;

    // See: https://wiki.libsdl.org/SDL_GL_SetSwapInterval
    // Set to adaptive Vsync, if that fails, set it to normal vsync.
    // If vsync is causing problems, see if point 3 helps:
    // https://www.khronos.org/opengl/wiki/Swap_Interval#Idiosyncrasies
    Log.i("Enabling adaptive VSync...");
    if (SDL_GL_SetSwapInterval(-1) != 0)
    {
        Log.i("Could not enable adaptive VSync, falling back on regular VSync...");
        SDL_ClearError();
        if (SDL_GL_SetSwapInterval(1) != 0)
        {
            printSdlError("Could not enable regular VSync:");
        }
    }


    // Load OpenGL
    Log.i("Initializing OpenGL...");
    GLSupport loadedGlVersion = loadOpenGL();
    if (loadedGlVersion == GLSupport.noLibrary ||
        loadedGlVersion == GLSupport.badLibrary ||
        loadedGlVersion == GLSupport.noContext)
    {
        import std.conv : to;
        Log.e("Failed to load OpenGL: " ~ loadedGlVersion.to!string);
        return exit();
    }

    glEnable(GL_DEPTH_TEST);

    static if (useKHRDebug)
    {
        Log.i("Setting debug message callback...");
        glEnable(GL_DEBUG_OUTPUT);
        glDebugMessageCallback(&logGlMessage, cast(void*)null);
    }

    SDL_GL_GetDrawableSize(window, &(renderBufferSize.x), &(renderBufferSize.y));

    Log.i("OpenGL level: %d.%d".format(loadedGlVersion / 10, loadedGlVersion % 10));

    // Initialize Resources
    Resources res = new Resources();

    import core.thread : Thread, dur; Thread.sleep(5.dur!"seconds");

    Log.i("Loading and setup complete.");

    Log.i("Creating MainMenuScreen...");

    // Create screen
    goToScreen(new MainMenuScreen(res));



    /***********************************************
     *                                             *
     *                  Game Loop                  *
     *                                             *
     ***********************************************/

    Log.i("Starting game loop...");

    ulong counterLast = 0;
    bool firstLoop = true;

    while (!quitRequested)
    {
        if (upcomingScreen !is null)
        {
            if (screen !is null)
            {
                screen.finalize();
            }

            screen = upcomingScreen;
            upcomingScreen = null;
        }

        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_QUIT:
                    requestQuit();
                    break;

                case SDL_KEYDOWN:
                    screen.keyDown(event.key.keysym);
                    break;

                case SDL_KEYUP:
                    screen.keyUp(event.key.keysym);
                    break;

                case SDL_MOUSEMOTION:
                    screen.mouseMotion(event.motion);
                    break;

                case SDL_MOUSEBUTTONDOWN:
                case SDL_MOUSEBUTTONUP:
                    screen.mouseButton(event.button);
                    break;

                case SDL_WINDOWEVENT:
                    switch (event.window.event)
                    {
                        case SDL_WINDOWEVENT_SIZE_CHANGED:
                            int w = event.window.data1;
                            int h = event.window.data2;
                            renderBufferSize = vec2i(w, h);
                            glViewport(0, 0, w, h);
                            screen.resize(renderBufferSize);
                            Log.i("Resized to %d×%d".format(w, h));
                            break;

                        default:
                            break;
                    }
                    break;

                default:
                    break;
            }
        }

        // Not sure if this is the best way of doing it, but it's more precise
        // than SDL_GetTicks(). It appears that the loop runs just about 60
        // times a second.
        ulong counterCurr = SDL_GetPerformanceCounter();
        ulong counterDiff = counterCurr - counterLast;
        real delta = (cast(real)counterDiff) / SDL_GetPerformanceFrequency();
        counterLast = counterCurr;

        if (firstLoop)
        {
            firstLoop = false;
            continue;
        }
        screen.idleUpdate(delta);

        glClearColor(0.1f, 0.4f, 0.6f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        screen.render();
        SDL_GL_SwapWindow(window);
    }

    exit();
}

} // end version(unittest) else block

private vec2i renderBufferSize;

public vec2i getRenderBufferSize()
{
    return renderBufferSize;
}


/**
 * Switch gameplay to the given screen.
 *
 * The current iteration of the main game loop will finish normally, and at the
 * start of the *next* iteration, this screen will be used instead.
 *
 * If this function is called multiple times within a single game loop, then
 * the most recent call is used.
 */
public void goToScreen(Screen screen)
{
    upcomingScreen = screen;
}
private Screen screen;
private Screen upcomingScreen;

private shared bool quitRequested = false;

/**
 * If this function is called, the program will exit once it finishes the
 * current iteration of the game loop.
 */
public void requestQuit()
{
    quitRequested = true;
}

private void exit()
{
    import std.stdio : writeln;

    Log.shutDownLog();
    writeln("Exiting.");
}
