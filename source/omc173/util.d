module omc173.util;

import omc173.theusual;

import std.range : iota;
import std.traits;

import datefmt : datefmt = format;


enum Direction : ubyte
{
    X_POS,
    X_NEG,
    Y_POS,
    Y_NEG,
    Z_POS,
    Z_NEG,
}

/// Returns `n` rounded up to the nearest multiple of `factor`. If `n` is
/// already a multiple of `factor`, the value returned will be equal to `n`.
public T roundUpTo(T)(T n, T factor)
if (isIntegral!T)
in(n >= 0, "`n` may not be negative")
in(factor > 0, "`factor` must be positive")
out(retVal; retVal >= n)
do
{
    return (
            (n / factor) +
            ((n % factor == 0) ? 0 : 1)
           ) * factor;
}
///
unittest
{
    writeln("TEST: roundUpTo()");

    assert(roundUpTo(44, 23) == 46);
    assert(roundUpTo(46, 23) == 46);
    assert(roundUpTo(0, 23) == 0);
    assert(roundUpTo(1, 23) == 23);
    assert(roundUpTo(23, 23) == 23);
}

/// Returns a static array whose contents are
/// [ 0, 1, 2, ... , N ]
public T[N] staticIota(T, size_t N)()
if (is(int : T))
{
    // There are probably better ways of doing this but I think this is good enough.
    import std.meta : aliasSeqOf;
    import std.range : iota;
    return [ aliasSeqOf!(N.iota) ];
}


enum BoxDimensions(T : Box!U, U...) = U[1];

/// Returns a translated box such that its center is located at `pos`.
/// If the box has integral coordinates, any odd dimensions will be rounded down
/// when calculating the new position. The box's volume will not be changed.
public B centerAt(B, T)(B box, T pos)
if (isBox!B && is(T == B.bound_t))
out(ret; ret.volume() == box.volume())
{
    B.bound_t newMin = pos - (box.size() / 2);
    return box.translate(newMin - box.min);
}
unittest
{
    writeln("TEST: centerAt()");

    box2i a = box2i(vec2i(0, 0), vec2i(1, 1));
    a = a.centerAt(vec2i(0, 0));
    assert(a.min == vec2i(0, 0));
    assert(a.center == vec2i(0, 0));

    box2i b = box2i(vec2i(0, 0), vec2i(2, 2));
    b = b.centerAt(vec2i(0, 0));
    assert(b.min == vec2i(-1, -1));
    assert(b.max == vec2i(1, 1));
    assert(b.center == vec2i(0, 0));

    box2f c = box2f(vec2f(-8, -8), vec2f(-4, -4));
    c = c.centerAt(vec2f(8, 8));
    assert(c.min == vec2f(6, 6));
    assert(c.max == vec2f(10, 10));
    assert(c.center == vec2f(8, 8));

    box3f d = box3f(vec3f(1, 1, 0), vec3f(2, 2, 2));
    d = d.centerAt(vec3f(0, 0, 0));
    assert(d.min == vec3f(-0.5, -0.5, -1));
    assert(d.max == vec3f(0.5, 0.5, 1));
    assert(d.center == vec3f(0, 0, 0));
}

/// Returns a translated box such that the middle of its top edge is located at
/// `pos`.
/// Only valid for 2D boxes.
/// If the box has integral coordinates, any odd dimensions will be rounded down
/// when calculating the new position. The box's area will not be changed.
public B topAt(B, T)(B box, T pos)
if (isBox!B && is(T == B.bound_t) && BoxDimensions!B == 2)
out(ret; ret.volume() == box.volume())
{
    B.bound_t currentTop = box.max;
    currentTop.x -= (box.width / 2);
    return box.translate(pos - currentTop);
}
unittest
{
    writeln("TEST: topAt()");

    box2i b = box2i(vec2i(0, 0), vec2i(2, 2));
    b = b.topAt(vec2i(0, 0));
    assert(b.min == vec2i(-1, -2));
    assert(b.max == vec2i(1, 0));

    box2i a = box2i(vec2i(0, 0), vec2i(1, 1));
    a = a.topAt(vec2i(0, 0));
    assert(a.min == vec2i(-1, -1));

    box2f c = box2f(vec2f(-8, -8), vec2f(-4, -4));
    c = c.topAt(vec2f(8, 8));
    assert(c.min == vec2f(6, 4));
    assert(c.max == vec2f(10, 8));
}


import std.datetime.systime : SysTime;

/// Gets the current system time and returns a string of it formatted using `formatTime`.
public string getTimestamp(string formatStr = "%F %T.%g", bool appendTZ = true)
{
    import std.datetime : Clock;
    SysTime curr = Clock.currTime();
    return formatTime(curr, formatStr, appendTZ);
}

/// Same as `getTimestamp`, but ensures the resulting string can exist in a file path.
///
/// See_Also:
///     - replaceInvalidPathChars
///     - getTimestamp
public string getTimestampForPath(string replaceWith = "", bool squashConsecutive = true)
{
    return replaceInvalidPathChars(getTimestamp("%F %T", true), replaceWith, squashConsecutive);
}

/// Takes a file name and returns a string equivalent to `str`, but any
/// characters that cannot exist in a file pathname will be removed or replaced.
///
/// Note that this means that path separators will be removed, since they are
/// invalid on both Windows and POSIX. Make sure the given string is only
/// a file name, not whole path.
///
/// The result is system dependent, and assumes the system is POSIX-compliant
/// if it's not Windows.
///
/// Params:
///
///     str = The string to clean up.
///
///     replaceWith = Invalid characters will be replaced with this string.
///                    Default is empty string.
///
///     squashConsecutive =  If true, consecutive invalid characters will be
///                           replaced by only one instance of `replaceWith`.
///                           Default is true.
///
public string replaceInvalidPathChars(string str, string replaceWith = "", bool squashConsecutive = true)
{
    import std.regex : regex, replaceAll, Regex, matchFirst;

    version(Windows)
    {
        auto toReplace = regex(`[><:"/\|?*]` ~ (squashConsecutive ? "+" : ""));
    }
    else
    {
        auto toReplace = regex(`[/]` ~ (squashConsecutive ? "+" : ""));
    }

    if (matchFirst(replaceWith, toReplace))
    {
        throw new Exception("Cannot replace invalid path characters with string \"" ~ replaceWith ~ "\" because it also contains invalid path characters.");
    }

    return replaceAll(str, toReplace, replaceWith);
}

unittest
{
    // TODO: Test on POSIX.
    version(Windows)
    {
        writeln("TEST: replaceInvalidPathChars()");

        string clean1 = replaceInvalidPathChars("abc??s");
        assert(clean1 == "abcs", "Received \"" ~ clean1 ~ "\".");

        string clean2 = replaceInvalidPathChars("abc??s", "_");
        assert(clean2 == "abc_s", "Received \"" ~ clean2 ~ "\".");

        string clean3 = replaceInvalidPathChars("abc??s", "_", false);
        assert(clean3 == "abc__s", "Received \"" ~ clean3 ~ "\".");

        try
        {
            replaceInvalidPathChars("abc", ":");
            assert(false, "Expected exception to be throw on trying to replace invalid path chars with \":\".");
        }
        catch (Exception e)
        {
            // Good.
        }
    }
}

/// Converts an instance of `SysTime` to a string with the following format:
/// YYYY-MM-DD HH:MM:SS TZ
/// where TZ is of the form ±HH:MM (UTC is +00:00).
///
/// Params:
///
///     time = The instance of `SysTime` to format.
///
///     formatStr = Formatting string. See datefmt docs for details.
///                  Default `%F %T.%g`.
///
///     appendTZ = If true, the resulting string will have the time zone
///                 appended at the end. Default true.
///
public string formatTime(SysTime time, string formatStr = "%F %T.%g", bool appendTZ = true)
{
    string retVal = datefmt(time, formatStr);

    if (appendTZ)
    {
        // datefmt normally outputs timezones in the format +hhmm,
        // but ISO says it should be +hh:mm, and I think that's more
        // consistent considering the timestamp is also colon-separated.
        string tz = datefmt(time, "%z");
        retVal ~= " " ~ tz[0..3] ~ ":" ~ tz[3..$];
    }

    return retVal;
}

unittest
{
    import std.datetime : DateTime, Duration, dur;
    import std.datetime.timezone : TimeZone, UTC, SimpleTimeZone;

    writeln("TEST: formatTime()");

    SysTime testUTC = SysTime(DateTime(2019, 11, 17, 20, 10, 35), dur!"msecs"(736), UTC());
    string expectedUTC = "2019-11-17 20:10:35.736 +00:00";
    string actualUTC = formatTime(testUTC);
    assert(actualUTC == expectedUTC, "Expected \"%s\", received \"%s\".".format(expectedUTC, actualUTC));

    immutable TimeZone est = new immutable SimpleTimeZone(dur!"hours"(-5), "EST");
    SysTime testEST = SysTime(DateTime(2019, 11, 17, 20, 10, 35), dur!"msecs"(736), est);
    string expectedEST = "2019-11-17 20:10:35.736 -05:00";
    string actualEST = formatTime(testEST);
    assert(actualEST == expectedEST, "Expected \"%s\", received \"%s\".".format(expectedEST, actualEST));

    immutable TimeZone cet = new immutable SimpleTimeZone(dur!"hours"(1), "CET");
    SysTime testCET = SysTime(DateTime(2019, 11, 17, 20, 10, 35), dur!"msecs"(736), cet);
    string expectCET = "2019-11-17 20:10:35.736 +01:00";
    string actualCET = formatTime(testCET);
    assert(actualCET == expectCET, "Expected \"%s\", received \"%s\".".format(expectCET, actualCET));

    immutable TimeZone npt = new immutable SimpleTimeZone(dur!"hours"(5) + dur!"minutes"(45), "NPT");
    SysTime testNPT = SysTime(DateTime(2019, 11, 17, 20, 10, 35), dur!"msecs"(736), npt);
    string expectNPT = "2019-11-17 20:10:35.736 +05:45";
    string actualNPT = formatTime(testNPT);
    assert(actualNPT == expectNPT, "Expected \"%s\", received \"%s\".".format(expectNPT, actualNPT));
}


/// Returns the length of the enum member with the longest name.
size_t lengthOfLongestMember(T)()
if(is(T == enum))
{
    import std.algorithm : map, maxElement;
    import std.format : format;
    import std.traits : EnumMembers;
    import std.uni : byGrapheme;
    import std.range : walkLength;

    return [EnumMembers!T]
           .map!((it) => "%s".format(it))
           .map!((it) => it.byGrapheme.walkLength)
           .maxElement;
}

unittest
{
    writeln("TEST: lengthOfLongestMember()");

    enum A
    {
        X,
        XX,
        XXX,
        B
    }
    assert(lengthOfLongestMember!A == 3);
}
