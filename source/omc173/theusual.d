/**
 * Public imports of a bunch of very commonly used modules.
 * A single `import omc173.theusual;` at the top of a module will include:
 *
 * - All of SDL
 * - All of OpenGL
 * - omc173 utility and debugging functions
 * - common and useful standard library functions.
 *
 *
 */
module omc173.theusual;

public import std.conv : to;
public import std.format : format;
public import std.path : buildNormalizedPath;
public import std.string : toStringz, fromStringz;

public import bindbc.sdl.bind;
public import bindbc.opengl.bind;

public import gfm.math;

public import enumap : Enumap, enumap; // Need to be specific since compiler doesn't like function name matching module name.

public import omc173.debugging;
public import omc173.logging;
public import omc173.ndarray;
public import omc173.resources;
public import omc173.util;

version(unittest)
{
    public import std.stdio : writeln;
}
