- [Programming](#programming)
  - [The D Programming Language](#the-d-programming-language)
  - [IDE](#ide)
  - [Setting up the Repository](#setting-up-the-repository)
  - [Tour of the Code](#tour-of-the-code)
  - [Code Style](#code-style)
  - [Debugging](#debugging)
- [Other Help Required](#other-help-required)

# Programming

The remainder of this section describes how to get the repository up and
running. Browse the project's [issues on Gitlab](https://gitlab.com/OpenMC173/openmc173/issues)
to see what you can help out with.

A 64-bit operating system is needed to run the game.


## The D Programming Language

OpenMC173 is [written in D](https://dlang.org/). If you're familiar with Java,
C++, C#, or any similar language, it won't take long to pick up D. That said,
it's quite a large language and can get complicated, so don't feel discouraged
if you feel lost looking at other people's code.

To get started, install the D toolchain on your computer.
[Download links can be found here.](https://dlang.org/download.html#dmd)

Some useful resources for D programming:
- [DLang Tour](https://tour.dlang.org/)
- [*Programming in D* by Ali Çehreli](https://ddili.org/ders/d.en/index.html)
- [Online D Runner](https://run.dlang.io)
- [Idioms for the D Programming Language by p0nce](https://p0nce.github.io/d-idioms/)


## IDE

I recommend **VS Code** with the `code-d` extension. In my experience, it's the
only IDE-like setup that's stable, up-to-date, and cross-platform.

More [editors](https://wiki.dlang.org/Editors) and [IDEs](https://wiki.dlang.org/IDEs).


## Setting up the Repository

1. Install [git lfs](https://git-lfs.github.com/).
2. Clone the repository: `git clone git@gitlab.com:OpenMC173/openmc173.git`.
3. Download SDL2 and the SDL_image add-on:
    * **Windows:**
        1. Download the SDL2 DLL [here](https://www.libsdl.org/download-2.0.php).  
           Extract the file `SDL2.dll` into the root of this repository.
        2. Download SDL_Image from [here](https://www.libsdl.org/projects/SDL_image/).  
           Extract the files `SDL2_image.dll`, `libpng16-16.dll`, and
           `zlib1.dll` into the root of this repository.
        3. Download SDL_Mixer from [here](https://www.libsdl.org/projects/SDL_mixer/).  
           Extract `SDL_mixer.dll` into the root of this repository.

    * **Linux:** SDL2 can be installed using your distribution's package
      manager. The required packages are `sdl2`, `sdl2_image`, and `sdl2_mixer`.
      The exact names may differ between platforms (e.g `libsdl2`).

    * **macOS:** Untested

4. **Linux:** You may need to install Mesa so that OpenGL can be linked by the
   build process.  
   On Ubuntu, run: `sudo apt install libgl1-mesa-dev`

5. Build and run the program:
    * **Linux & macOS:** `dub build` builds without running, `dub run` builds
      then runs.
    * **Windows (64-bit):** `dub build --arch=x86_64` builds without running,
      `dub run --arch=x86_64` builds then runs.


## Tour of the Code

*To-do*


## Code Style

- Most in-code identifiers (variables, functions, etc.) are in `camelCase`.
- Enum constants (both kinds) in `SCREAMING_SNAKE_CASE`.
- Classes, structs, enum types, etc. (anything that is a type) are in
  `PascalCase`.
- Template parameters should be `SCREAMING_SNAKE_CASE` if values, and single
  uppercase letters if types.
- Module and package names are in `snake_case` and should not contain uppercase
  letters.
- Opening braces go on their own line.
- `unittest` blocks come immediately after the function they test.
- The shorter the line, the better, but there's no hard limit except for
  comments (keep them below 80 columns).
- For function declarations and function calls with lots of arguments, use
  something like the following format:  
    ```d
    void someFunction(
        string name,
        Color color,
        float size,
        int x, int y,
        bool hidden = false
    )
    {
        // ...
    }

    someFunction(
        "Sam",
        Color("#c0ffee"),
        1.0f,
        23, 12,
        true
    );
    ```

If in doubt, follow the [D Style conventions](https://dlang.org/dstyle.html).

The goal of all this is to make the code readable, so if breaking any of these
rules results in prettier and more readable code, then do it. For example, when
writing a lambda function, it might look nicer to have the opening brace on the
same line as the rest of the statement.


## Debugging


<!-- omit in toc -->
### VS Code users on Linux

Use the Native Debug extension: `ext install webfreak.debug`


<!-- omit in toc -->
### Everyone else

I had lots of trouble trying to debug D code on Windows, or with other IDEs, but
if you have had any luck then I would be very grateful if you could add the
instructions here :)


<!-- omit in toc -->
### General Tips

If you run into issues while debugging, try these:

- Instead of dmd, use the [ldc2 compiler](https://github.com/ldc-developers/ldc)
  (it may output better debug information).
- Build using the `debug-deoptimized` build type.


# Other Help Required

Eventually we will need translators, artists, and so on, but nothing specific
for now. Feel free to open up an issue if you have something to add!
