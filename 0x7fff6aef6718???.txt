0x7fff6aef6718
0x7fff6aef6718

The segfault occurs at an instruction that looks like this:

movq   %rdi, 0x8(%rsp)

???
How can the stack pointer be at a protected address???
Is this a bug in the radeon driver.


Stack size is only ~12 MB


//////////////////////

Works when loading in Resources, but not when loading in Shaders...
???


NO!!!

Actually it's the 2D shader that's failing!


////////////////////////////////////////////////////////


SIFDUHsdfgsykdgfag,wkaef


Ok so the following file is fine:

```
#version 330

precision highp float;

uniform mat4 mvp;

in vec3 position;
in vec2 texCoord;
in vec2 texSrcPos;
in vec2 texSrcSize;

out vec2 fTexCoord;


void main(void)
{
    fTexCoord = vec2(texSrcPos.x + (texCoord.x * texSrcSize.x),
                     texSrcPos.y + (texCoord.y * texSrcSize.y));
    gl_Position = mvp * vec4(position, 1.0);
}
```

but the following is not:

```
#version 330

precision highp float;

uniform mat4 mvp;


in vec3 position;
in vec2 texCoord;
in vec2 texSrcPos;
in vec2 texSrcSize;

out vec2 fTexCoord;


void main(void)
{
    fTexCoord = vec2(texSrcPos.x + (texCoord.x * texSrcSize.x),
                     texSrcPos.y + (texCoord.y * texSrcSize.y));
    gl_Position = mvp * vec4(position, 1.0);
}
```


What??????????????????????
The only difference is an extra line break between the uniform and the ins.
(╯°□°)╯︵ ┻━┻

...but not in the test project




//////////////////////////////

Ok so when changing '2D.vert' so that it uses 'position' for 'gl_Position', it works, but when using instance_pos, it doesn't. ???
