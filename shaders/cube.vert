#version 330

precision highp float;

uniform mat4 mvp;

in vec3 position;
in vec2 texCoord;
in vec2 texSrcPos;
in vec2 texSrcSize;

out vec2 fTexCoord;


void main(void)
{
    fTexCoord = vec2(texSrcPos.x + (texCoord.x * texSrcSize.x),
                     texSrcPos.y + (texCoord.y * texSrcSize.y));
    gl_Position = mvp * vec4(position, 1.0);
}
