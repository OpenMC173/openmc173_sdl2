#version 330

precision highp float;

uniform sampler2D texture;
uniform vec4 multColor;

in vec2 fTexCoord;

out vec4 finalColor;


void main(void)
{
    // This works somehow.
    finalColor = multColor * texture2D(texture, fTexCoord);
    if (finalColor.a <= 0) discard;
}
