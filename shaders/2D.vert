#version 330

precision highp float;

uniform mat4 mvp;
uniform int columns;
uniform vec2 offset;

in vec3 position;
in vec2 texCoord;
in vec2 texSrcPos;
in vec2 texSrcSize;

out vec2 fTexCoord;


void main(void)
{
    float col = mod(gl_InstanceID, columns);
    int row = gl_InstanceID / columns;
    float frow = float(row);
    float px = position.x /* + col * offset.x */;
    float py = position.y /* + frow * offset.y */;
    fTexCoord = vec2(texSrcPos.x + (texCoord.x * texSrcSize.x),
                     texSrcPos.y + (texCoord.y * texSrcSize.y));
    gl_Position = mvp * vec4(
        px,
        py,
        position.z,
        1.0
    );
    // gl_Position = mvp * vec4(instance_pos, 1.0);
}
